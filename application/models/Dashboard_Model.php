<?php
	class Dashboard_Model extends CI_Model {
		public function __construct() {
			parent::__construct();
		}
		
		public function ValidateUser($EmployeeID, $Password) {
			$result = $this->db->query("Exec OM_LoginAccess '$EmployeeID', '$Password'");

			if (!$result)
			{
				$error = $this->db->error();
				$response['error'] = preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else
			{
				$result = $result->result_array();
				if (!empty($result))
				{
					$response['AuthenticUser'] = $result[0];
				}
				else
				{
					$response['error'] = 'No user found';
				}
			}

			return $response;
		}
		
		public function checkLoginFromMkonnect($username) {
			$query = 
			"
				SELECT
					ew.PID,
					ew.EmployeeID,
					u.UserName,
					ew.FullName [EmployeeName],
					CONVERT(INT, SUBSTRING((CASE WHEN ew.EmployeeID = '600000' THEN 'L12' WHEN lu.LookUpValue = 'N/A' AND ew.EmployeeID != '600000' THEN 'L0' ELSE lu.LookUpValue END), 1+LEN('L'), LEN(lu.LookUpValue))) [Level],
					lu1.LookupValue [Team],
					ea.ProjectCode,
					(
						CASE
							WHEN lu2.LookupValue LIKE '%head%' THEN 1
							ELSE 0
						END
					) [IsDivHead]
				FROM Timesheet_Staging..EmployeeWorkforce ew WITH(NOLOCK)
				LEFT JOIN Timesheet_Staging..EmployeeAllocation ea WITH(NOLOCK) ON ew.PID = ea.PID
				LEFT JOIN Timesheet_Staging..HRMSLookUp lu WITH(NOLOCK) ON ea.EmployeeLevelID = lu.LookupID
				LEFT JOIN Timesheet_Staging..HRMSLookUp lu1 WITH(NOLOCK) ON ea.TeamNameID = lu1.LookupID
				LEFT JOIN Timesheet_Staging..HRMSLookUp lu2 WITH(NOLOCK) ON ea.DesignationID = lu2.LookupID
				LEFT JOIN Timesheet..Users u WITH(NOLOCK) ON ew.EmployeeID COLLATE DATABASE_DEFAULT = u.EmployeeNo COLLATE DATABASE_DEFAULT 
				WHERE u.UserName = '$username'
			";

			$result = $this->db->query($query);

			if (!$result)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else
			{
				$result = $result->result_array();
				if (!empty($result)) {
					$response['AuthenticUser'] = $result[0];
				}
				else {
					$response['error'] = 'No user found';
				}
			}

			return $response;
		}
		public function getEligibleForNomination(){
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("Exec omGetEligibleForNomination $PID")->result();
			return $result;
		}
		
		public function getEmployeeDeps() {

			$employeeID = $this->session->userdata('EmployeeID');
			$employeePID = $this->session->userdata('PID');
			$result1 = $this->db->query("EXEC OM_GetEmployeeDetails '$employeeID'");
			if (!$result1)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->result_array();

				if (!empty($results)) {						
					$response['empDetails'] = $results;					
				}
				else {
					$response['error'] = 'Employee Details Not found...';
				}
			}

			$result2 = $this->db->query("Exec OM_GetTeamDetails $employeePID");
			if (!$result2)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$empList = $result2->result_array();

				if (!empty($empList)) {						
					$response['teamList'] = $empList;					
				}
				else {
					$response['teamList'] = '';
				}
			}

			$result3 = $this->db->query("SELECT * FROM ContentProvider WHERE IsActive = 1");
			if (!$result3) {
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$content_provider = $result3->result_array();

				if (!empty($results)) {					
					$response['content_provider'] = $content_provider;									
				}
				else {
					$response['error'] = 'Employee Details Not found...';
				}
			}
			return $response;
		}

		/*
		public function getNominationsList($userType) {
			$employeePID = $this->session->userdata('PID');
			if($userType == 'Self') {
				$result1 = $this->db->query("EXEC OM_GetNominationDetails '$employeePID'");
				if (!$result1)
				{
					$error = $this->db->error();
					$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
				}
				else {
					$results = $result1->result_array();

					if (!empty($results)) {						
						$response['empDetails'] = $results;					
					}
					else {
						$response['error'] = 'Employee Details Not found...';
					}
				}		
			}
			else {
				$result1 = $this->db->query("EXEC OM_GetTeamNominationDetails '$employeePID'");
				if (!$result1)
				{
					$error = $this->db->error();
					$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
				}
				else {
					$results = $result1->result_array();

					if (!empty($results)) {						
						$response['empDetails'] = $results;					
					}
					else {
						$response['error'] = 'Employee Details Not found...';
					}
				}
			}
			
			
			

			return $response;
		}

		public function getNominationList($start, $limit, $search, $regex, $isadmin, $ishr) {
			$response = array();
			$employeePID = $this->session->userdata('PID');
			if($isadmin == 1 || $ishr == 1) {
				if ($search != '') {
					$result = $this->db->query("EXEC OM_GetAllNominations @EmpPID=$employeePID, @Start=$start, @limit=$limit, @Search='$search'");
				}
				else {				
					$result = $this->db->query("EXEC OM_GetAllNominations @EmpPID=$employeePID, @Start=$start, @limit=$limit");
				}
				$response['nominations'] = array();
				if (!$result) {
					$error = $this->db->error();
					$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
				}
				else {
					$results = $result->result_array();
	
					if (!empty($results)) {
						$response['nominations'] = $results;
						if ($search != '') {
							$response['total'] = $this->db->query("EXEC OM_GetAllNominations @EmpPID=$employeePID, @Start=$start, @limit=$limit, @Search='$search', @FullCount='Y'")->row()->Total;
						}
						else {					
							$response['total'] = $this->db->query("EXEC OM_GetAllNominations @EmpPID=$employeePID, @Start=$start, @limit=$limit, @FullCount='Y'")->row()->Total;						
						}
					}
					else {
						$response['nominations'] = array();
					}
					
				}
			}
			else {
				if ($search != '') {
					$result = $this->db->query("EXEC OM_GetEmpNominationDetails @EmpPID=$employeePID, @Start=$start, @limit=$limit, @Search='$search'");
				}
				else {				
					$result = $this->db->query("EXEC OM_GetEmpNominationDetails @EmpPID=$employeePID, @Start=$start, @limit=$limit");
				}
				$response['nominations'] = array();
				if (!$result) {
					$error = $this->db->error();
					$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
				}
				else {
					$results = $result->result_array();
	
					if (!empty($results)) {
						$response['nominations'] = $results;
						if ($search != '') {
							$response['total'] = $this->db->query("EXEC OM_GetEmpNominationDetails @EmpPID=$employeePID, @Start=$start, @limit=$limit, @Search='$search', @FullCount='Y'")->row()->Total;
						}
						else {					
							$response['total'] = $this->db->query("EXEC OM_GetEmpNominationDetails @EmpPID=$employeePID, @Start=$start, @limit=$limit, @FullCount='Y'")->row()->Total;						
						}
					}
					else {
						$response['nominations'] = array();
					}
					
				}
			}
			
			return $response;
		}

		public function createNomination($formData) {
			$empPID = $formData['wizard_EmpPID'];
			$rm1PID = $formData['wizard_RM1PID'];
			$dhPID = $formData['wizard_DHPID'];
			$teamID = $formData['wizard_TeamID'];
			$divisionID = $formData['wizard_DivisionID'];
			$content_provider = $formData['wizard_content_provider'];
			$course_name = $formData['wizard_course_name'];
			$course_url = $formData['wizard_course_url'];
			$course_type = $formData['wizard_course_type'];			
			$course_payment_type = isset($formData['wizard_payment_type']) ? $formData['wizard_payment_type'] : '';
			$course_paid_by = isset($formData['wizard_payment_detail']) ? $formData['wizard_payment_detail'] : '';
			$course_duration = $formData['wizard_course_duration'];
			$course_cost = isset($formData['wizard_course_cost']) ? $formData['wizard_course_cost'] : '';
			$course_option_detail = $formData['wizard_course_option_detail'] ? $formData['wizard_course_option_detail'] : '';
			$course_inst_count = $formData['wizard_course_installment_count'] ? $formData['wizard_course_installment_count'] : 0;
			// $course_inst_amount = $formData['wizard_course_installment_amount'] ? $formData['wizard_course_installment_amount'] : 0 ;

			$instalments = $formData['instalments'];

			$course_st_date = date('Y-m-d', strtotime($formData['wizard_course_st_date']));
			$course_end_date = date('Y-m-d', strtotime($formData['wizard_course_end_date']));
			$raisedID = $formData['wizard_EmpPID'];
			$raised_on = date('Y-m-d', time());
			$emp_comments = $formData['wizard_empComments'];
			$type = 'Self';

			// $result1 = $this->db->query("EXEC Create_Nomination $empPID,$rm1PID, $dhPID, $teamID, $divisionID, '$content_provider', '$course_name', $course_cost,  '$course_duration', '$course_st_date', '$course_end_date', $raisedID, '$raised_on','$emp_comments', '$type', '$course_type', '$course_payment_type', '$course_paid_by','$course_option_detail', $course_inst_count, $course_inst_amount");

			$result1 = $this->db->query("EXEC Create_Nomination $empPID,$rm1PID, $dhPID, $teamID, $divisionID, '$content_provider', '$course_name', $course_cost,  '$course_duration', '$course_st_date', '$course_end_date', $raisedID, '$raised_on','$emp_comments', '$type', '$course_type', '$course_payment_type', '$course_paid_by','$course_option_detail', $course_inst_count, '$instalments'");

			if (!$result1)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else
			{
				$response['status'] = 'Updated course request';
			}

			return $response;
		}

		public function nominateEmp($formData) {
			$employeePID = $this->session->userdata('PID');
			$empPID = $formData['employee']['EmpPID'];
			$rm1PID = $formData['employee']['RM1PID'];
			$dhPID = $formData['employee']['DHPID'];
			$teamID = $formData['employee']['TeamID'];
			$divisionID = $formData['employee']['DivisionID'];
			$content_provider = $formData['course']['content_provider'];
			$course_name = $formData['course']['course_name'];
			$course_cost = isset($formData['course']['course_cost']) ? $formData['course']['course_cost'] : 0;
			$course_type = $formData['course']['course_type'];
			$course_payment_type = $formData['course']['payment_type'];
			$course_duration = $formData['course']['course_duration'];
			$course_st_date = date("Y-m-d", strtotime($formData['course']['course_st_dt']));
			$course_end_date = date("Y-m-d", strtotime($formData['course']['course_end_dt']));
			$raisedID = $employeePID;
			$raised_on = date('Y-m-d', time());
			$comments = $formData['course']['dhComments'];
			$course_paid_by =  $formData['course']['payment_detail'];
			$course_option_detail =  isset($formData['course']['option_detail']) ? $formData['course']['option_detail'] : '';
			$course_inst_count =  isset($formData['course']['installment_count']) ? $formData['course']['installment_count'] : 0;
			// $course_inst_amount =  isset($formData['course']['installment_amount']) ? $formData['course']['installment_amount'] : 0;

			$installments = $formData['instalments'];

			$training_type =  $formData['course']['training_type'];
			$type = 'ByOthers';

			$result1 = $this->db->query("EXEC Create_Nomination @EmployeePID = $empPID, @RM1PID = $rm1PID, @DHPID = $dhPID, @TeamID = $teamID, @DivisionID = $divisionID, @ContentProvider = '$content_provider', @CourseName = '$course_name', @CourseCosting = '$course_cost', @CourseDuration = '$course_duration', @CourseStartDate = '$course_st_date', @CourseEndDate = '$course_end_date', @RaisedBy = $raisedID, @RaisedOn = '$raised_on', @Comments = '$comments', @Type = '$type', @CourseType = '$course_type', @PaymentType = '$course_payment_type', @PaidBy = '$course_paid_by', @CourseOptions = '$course_option_detail', @CourseInstCount = $course_inst_count, @Instalments = '$installments', @TrainingType = '$training_type'");

			if (!$result1)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else
			{
				$response['status'] = 'Course request successfully updated.';
				
			}

			return $response;
		}
		*/

		public function getTeamCount() {
			$employeePID = $this->session->userdata('PID');
			
			$result1 = $this->db->query("EXEC OM_GetTeamDetails '$employeePID', 'Y'");
			if (!$result1)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->result_array();

				if (!empty($results)) {						
					$response['count'] = $results;					
				}
				else {
					$response['error'] = 'Employee Details Not found...';
				}
			}		

			return $response;
		}

		public function getAccessStatus() {
			$employeePID = $this->session->userdata('PID');
			
			$result1 = $this->db->query("EXEC OM_GetAccessStatus $employeePID");
			if (!$result1)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->result_array();

				if (!empty($results)) {						
					$response['acessStatus'] = $results;					
				}
				else {
					$response['error'] = 'Employee Details Not found...';
				}
			}		

			return $response;
		}
		/*
		public function getApprovedCourses() {
			$employeePID = $this->session->userdata('PID');

			$result1 = $this->db->query("EXEC OM_GetApprovedCourses $employeePID");
			if (!$result1)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->result_array();

				if (!empty($results)) {						
					$response['course_list'] = $results;					
				}
				else {
					$response['error'] = 'Approved courses not found...';
				}
			}		

			return $response;
		}

		public function getDocuments($start, $limit, $search, $regex) {
			$employeePID = $this->session->userdata('PID');

			if ($search != '') {
				$result = $this->db->query("EXEC OM_GetDocuments @EmpPID=$employeePID, @Start=$start, @limit=$limit, @Search='$search'");
			}
			else {				
				$result = $this->db->query("EXEC OM_GetDocuments @EmpPID=$employeePID, @Start=$start, @limit=$limit");
			}
			$response['documents'] = array();
			if (!$result) {
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result->result_array();

				if (!empty($results)) {
					$response['documents'] = $results;
					if ($search != '') {
						$response['total'] = $this->db->query("EXEC OM_GetDocuments @EmpPID=$employeePID, @Start=$start, @limit=$limit, @Search='$search', @FullCount='Y'")->row()->Total;
					}
					else {					
						$response['total'] = $this->db->query("EXEC OM_GetDocuments @EmpPID=$employeePID, @Start=$start, @limit=$limit, @FullCount='Y'")->row()->Total;						
					}
				}
				else {
					$response['documents'] = array();
				}
				
			}

			return $response;
		}

		public function getEmpHeadDetails($empPID) {
			$result1 = $this->db->query("EXEC OM_GetEmployeeHeadDetails '$empPID'");
			if (!$result1) {
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->row();

				if (!empty($results)) {					
					$response['empHeadDet'] = $results;									
				}
				else {
					$response['error'] = 'Employee Details Not found...';
				}
			}

			return $response;
		}

		public function getEmpNominationData($nominationID, $empPID) {
			$result1 = $this->db->query("EXEC OM_GetNominationDetail $nominationID, $empPID");
			if (!$result1) {
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->row();

				if (!empty($results)) {					
					$response['empNominationData'] = $results;									
				}
				else {
					$response['error'] = 'Employee Details Not found...';
				}
			}

			return $response;
		}
		*/
		function getInstalments($nominationID) {
			$result = $this->db->query("EXEC omGetInstallments $nominationID")->result();
			return $result;
		}

		function updateInstallment($id, $amount) {
			$result = $this->db->query("EXEC omUpdateInstallment $id, $amount")->result();
			return $result;
		}

		function updateInstallmentStatus($id, $statusId) {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omUpdateInstallmentStatus $id, $statusId, $PID")->result();
			return $result;
		}
		
		function updateInstallments($installments) {
			$result = $this->db->query("EXEC omUpdateInstallments '$installments'")->result();
			return $result;
		}

		function updateInstallmentReceiptStatus($id, $statusId) {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omUpdateInstallmentReceiptStatus $id, $statusId, $PID")->result();
			return $result;
		}

		function updateInstallmentReceipt($installmentId, $fileName){
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omUpdateInstallmentReceipt $installmentId, '$fileName', $PID")->result() ;
			return $result;
		}
		function uploadCertificate($postValues, $fileName){
			$PID = $this->session->userdata('PID');
			$nominationId = $postValues['nominationId'];
			$comments = $postValues['comments'];
			$result = $this->db->query("EXEC omuploadCertificate $nominationId, '$fileName', $PID, '$comments'")->result();
			return $result;
		}
		function getInstallmentStatus() {
			$result = $this->db->query("EXEC omGetInstallmentStatus")->result();
			return $result;
		}
		/*
		public function updateNominationDetail($nominationID, $commentsBy, $comments, $additionalCost, $acceptStatus, $training_type, $dhPID) {
			$additionalCost = 0;
			if($commentsBy == 'DH') {
				$result1 = $this->db->query("EXEC OM_UpdateNominationDetail $nominationID, '$comments', '$additionalCost', $commentsBy, $acceptStatus, $training_type, $dhPID");
			}
			else {
				$result1 = $this->db->query("EXEC OM_UpdateNominationDetail $nominationID, '$comments', $additionalCost, $commentsBy");	
			}

			if (!$result1) {
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->result_array();
				$response['status'] = 'Nomination details have been updated.';	
			}

			return $response;
		}

		public function uploadFileReceipt($fileType, $nominationID, $employeePID, $uploadData, $emPID){
			
			$result1 = $this->db->query("EXEC Create_Document $fileType, $nominationID, $employeePID, '$uploadData', $emPID");
			if (!$result1) {
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$response['upload_status'] = 'File uploaded successfully!!!';	
			}

			return $response;
		}

		

		public function getNominatedDocument($documentID, $empPID) {
			$result1 = $this->db->query("EXEC OM_GetNominationDocumentByID $documentID, $empPID");
			if (!$result1) {
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->row();

				if (!empty($results)) {					
					$response['empDocumentData'] = $results;									
				}
				else {
					$response['error'] = 'Employee Details Not found...';
				}
			}

			return $response;
		}

		public function getdashboardList() {
			$employeePID = $this->session->userdata('PID');

			$result1 = $this->db->query("EXEC OM_GetDashboardDetails $employeePID");
			if (!$result1)
			{
				$error = $this->db->error();
				$response['error'] = '<br>'.preg_replace('/[\[{\(].*[\]}\)]/U' , '', $error['message']);
			}
			else {
				$results = $result1->result_array();

				if (!empty($results)) {						
					$response['dashboard_list1'] = $results;					
				}
				else {
					$response['error'] = 'Approved courses not found...';
				}
			}		

			return $response;
		}
		*/
		function getAccessType(){
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omGetAccessType $PID")->result();
			return $result;
		}

		function getContentProviders() {
			$result = $this->db->query("EXEC omGetContentProviders")->result();
			return $result;
		}

		function getNominationsCount() {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omGetNominationsCount $PID,'overall'")->result();
			return $result;
		}
		/* chart functionalities starts --> karthik */
		function getContentProvidersCount(){
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omGetContentProvidersCount $PID")->result();
			return $result;
		}
		function getDivisionWiseNominationCount(){
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omGetDivisionWiseNominationCount $PID")->result_array();
			return $result;
		}
		function getExitEmpList(){
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omGetEmployeeSeparation $PID")->result_array();
			return $result;
		}
		function getDivisionWiseNominationStatistics(){
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omGetNominationsCount $PID,'divisionwise'")->result_array();
			return $result;
		}
		function getDivisionWiseBudget($Year) {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC getDivisionHeadsBudget $PID,'$Year'")->result_array();
			return $result;
		}
		function getDivisionHeadsBudget() {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC getDivisionHeadsBudget $PID")->result_array();
			return $result;
		}
		/* chart functionalities ends --> karthik */
		function getDivisionHeadBudget() {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC getDivisionHeadBudget $PID")->result_array();
			return $result;
		}

		function getNominations($OptionalCondition) {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omGetNominations $PID,'$OptionalCondition'")->result();
			return $result;
		}

		function applyCourse($courseInformation) {
			$createdBy = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omInsertNomination '$courseInformation', $createdBy")->result();
			return $result;
		}

		function updateNomination($nomination) {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omUpdateNomination '$nomination', $PID")->result();
			return $result;
		}

		/**
		 * To be removed
		 */
		/*
		function cancelNomination($nominationId, $comments) {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omCancelNomination $nominationId, '$comments', $PID")->result();
			return $result;
		}
		*/

		function getNomination($nominationId) {
			$result = $this->db->query("EXEC omGetNomination $nominationId")->result();
			return $result;
		}

		function getNominationTimeline($nominationId) {
			$result = $this->db->query("EXEC omGetNominationTimeline $nominationId")->result();
			return $result;
		}
		
		function updateNominationStatus($nominationId, $comments, $status) {
			$PID = $this->session->userdata('PID');
			$result = $this->db->query("EXEC omUpdateNominationStatus $nominationId, '$comments', '$status', $PID")->result();
			return $result;
		}

		
		
}
?>
