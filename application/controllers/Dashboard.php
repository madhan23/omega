<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/home
	 *	- or -
	 * 		http://example.com/index.php/home/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->view('home');
	}
	
	public function sidebar()
	{
		$this->load->view("default/sidebar/main_sidebarView");
	}

	public function sidebarCtrl()
	{
		$this->load->view("default/sidebar/main_sidebarController.js");
	}
	
	public function header()
	{
		$this->load->view("default/header/main_headerView");
	}

	public function headerCtrl()
	{
		$this->load->view("default/header/main_headerController.js");
	}
	
	public function restricted()
	{
		$this->load->view("default/restricted");
	}
	
	public function login()
	{
		$this->load->view("login/loginController.js");
	}

	public function login_view()
	{
		$this->load->view("login/loginView");
	}

	public function getLogin() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);
		$EmployeeID = isset($request_pay_load['EmployeeID']) ? $request_pay_load['EmployeeID'] : '';
		$Password = isset($request_pay_load['Password']) ? $request_pay_load['Password'] : '';

		if ($EmployeeID === '' || $Password === '')
		{
			$response['error'] = 'Unable to process the request. Mandatory data missing';
		}
		else
		{
			$response = $this->Dashboard_Model->ValidateUser($EmployeeID, $Password);
			if (isset($response['AuthenticUser']))
			{
				$AuthenticUser = $response['AuthenticUser'];
				$this->session->set_userdata($AuthenticUser);
				$response['AuthenticUser']['accessType'] = $this->Dashboard_Model->getAccessType()[0]->accessType;
				$this->session->set_userdata('accessType', $response['AuthenticUser']['accessType']);

			}
		}

		echo json_encode($response);
	}
	
	public function logout() {	
		$this->session->sess_destroy();
		redirect(base_url().'#/login');
	}
	
	function checkValidDate($date)
	{
		if(!empty($date))
		{
			if (strtotime($date) <= strtotime(Date('d-m-Y')))
				return true;
			else
			{
				$this->form_validation->set_message('checkValidDate','The {field} field must be less than or equal from current date.');
				return false;
			}
		}
	}

	// Validating Employee Login
	public function Check_Login() {
		$response['AuthenticUser'] = $this->session->userdata('EmployeeID') ? array(
			'PID' => $this->session->userdata('PID'),
			'EmployeeID' => $this->session->userdata('EmployeeID'),
			'UserName' => $this->session->userdata('UserName'),
			'EmployeeName' => $this->session->userdata('EmployeeName'),
			'Level' => $this->session->userdata('Level'),
			'Team' => $this->session->userdata('Team'),
			'IsDivHead' => $this->session->userdata('IsDivHead'),
			'TeamCount' => $this->session->userdata('TeamCount'),
			'accessType' => $this->session->userdata('accessType'),
		) : null;
		echo json_encode($response);
	}

	function loginFromMkonnect()
	{
		if($name = $this->input->get('name')) {
			$username = decrypt(preg_replace('/\s/', '+', $name));
			$response = $this->Dashboard_Model->checkLoginFromMkonnect($username);
			if (isset($response['AuthenticUser']))
			{
				$AuthenticUser = $response['AuthenticUser'];
				$this->session->set_userdata($AuthenticUser);
				$AuthenticUser['accessType'] = $this->Dashboard_Model->getAccessType()[0]->accessType;
				$this->session->set_userdata('accessType', $AuthenticUser['accessType']);
			}
			redirect(base_url().'#login');
		}
		else
		{
			echo 'Invalid Request';
			die();
		}
	}

	/********* Nomination Start *********/
	public function nomination()
	{
		$this->load->view("nomination/nominationController.js");
	}

	public function nomination_view()
	{
		$this->load->view("nomination/nominationView");
	}

	public function nominateEmployee() {
		$this->load->view("nomination/nominateEmployeeController.js");
	}
	public function nominateEmployee_view() {
		$this->load->view("nomination/nominateEmployeeView");
	}

	public function courseApplication() {
		$this->load->view("nomination/courseApplicationController.js");
	}
	public function courseApplication_view() {
		$this->load->view("nomination/courseApplicationView");
	}

	public function uploadDocuments() {
		$this->load->view("nomination/uploadDocumentsController.js");
	}

	public function uploadDocuments_view() {
		$this->load->view("nomination/uploadDocumentsView");
	}
	public function getEmployeeDeps() {
		if ($this->session->userdata('EmployeeID')) {
			$response = $this->Dashboard_Model->getEmployeeDeps();
		}
		else {
			$response['error'] = 'Session expired. Please login to proceed';
		}
		echo json_encode($response);		
	}

	public function getNominationsList() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);
		if ($this->session->userdata('EmployeeID')) {
			if($request_pay_load != '') {
				$userType = isset($request_pay_load['userType']) ? $request_pay_load['userType'] : '';
				$response = $this->Dashboard_Model->getNominationsList($userType);
			}
			else {
				$response['error'] = 'Could not process the request. Mandatory data missing';				
			}
		}
		else {
			$response['error'] = 'Session expired. Please login to proceed';
		}
		echo json_encode($response);
	}

	public function addCourseRequest() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);

		$xml = new SimpleXMLElement('<instalments/>');
		array2XML($xml, $request_pay_load['formData']['installments']);
		$request_pay_load['formData']['instalments'] = $xml->asXML();
		
		if($request_pay_load != '') {
			// $formData = json_decode($request_pay_load['formData'], true);
			$formData = $request_pay_load['formData'];
			$response = $this->Dashboard_Model->createNomination($formData);
		}
		else {
			$response['error'] = 'Could not process the request. Mandatory data missing';			
		}
		echo json_encode($response);
	}

	public function nominateEmp() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);

		$xml = new SimpleXMLElement('<instalments/>');
		array2XML($xml, $request_pay_load['formData']['installments']);
		$request_pay_load['formData']['instalments'] = $xml->asXML();
			
		if($request_pay_load != '') {
			$formData = $request_pay_load['formData'];			
			$response = $this->Dashboard_Model->nominateEmp($formData);
		}
		else {
			$response['error'] = 'Could not process the request. Mandatory data missing';			
		}
		echo json_encode($response);
	}

	public function getTeamCount() {
		if ($this->session->userdata('EmployeeID')) {
			$response = $this->Dashboard_Model->getTeamCount();
		}
		else {
			$response['error'] = 'Session expired. Please login to proceed';
		}
		echo json_encode($response);
	}

	public function getEmpHeadDetails() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);		
		if($request_pay_load != '') {
			$empPID = isset($request_pay_load['empPID']) ? $request_pay_load['empPID'] : '';
			$response = $this->Dashboard_Model->getEmpHeadDetails($empPID);
		}
		else {
			$response['error'] = 'Could not process the request. Mandatory data missing';			
		}
		echo json_encode($response);
	}

	public function getEmpNominationData() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);
				
		if($request_pay_load != '') {
			$nominationID = isset($request_pay_load['nominationID']) ? $request_pay_load['nominationID'] : '';
			$empPID = isset($request_pay_load['empPID']) ? $request_pay_load['empPID'] : '';
			$response = $this->Dashboard_Model->getEmpNominationData($nominationID, $empPID); 
			$response['instalments'] = $this->Dashboard_Model->getInstalments($nominationID);
		}
		else {
			$response['error'] = 'Could not process the request. Mandatory data missing';			
		}
		echo json_encode($response);
	}

	public function getNominationList() {

		$ePID = $this->session->userdata('PID');

		$request_pay_load = json_decode(file_get_contents('php://input'), true);
		$start = isset($request_pay_load['start']) ? $request_pay_load['start'] : '';
		$limit = isset($request_pay_load['limit']) ? $request_pay_load['limit'] : '';
		$search = isset($request_pay_load['search']['value']) ? $request_pay_load['search']['value'] : '';
		$regex = isset($request_pay_load['search']['regex']) ? $request_pay_load['search']['regex'] : false;
		if($ePID == '13815' || $ePID == '11348' || $ePID == '10888') {
			$isadmin = 1;
			$ishr = 1;			
		}
		else {
			$isadmin = isset($request_pay_load['isadmin']) ? $request_pay_load['isadmin'] : '';
			$ishr = isset($request_pay_load['ishr']) ? $request_pay_load['ishr'] : '';			
		}
		
		$response = array();
		
		$response_n = $this->Dashboard_Model->getNominationList($start, $limit, $search, $regex, $isadmin, $ishr);
		
		if (isset($response_n['nominations']) && !empty($response_n['nominations']))
		{
			$indnominations = $response_n['nominations'];

			$nomination_array = array_values($indnominations);	
			
			$PID = $this->session->userdata('PID');

			foreach($nomination_array as $key => $nomination)
			{
				$empNominations[$key]['EmployeePID'] = utf8_encode($nomination_array[$key]['EmployeePID']);
				$empNominations[$key]['EmployeeID'] = utf8_encode($nomination_array[$key]['EmployeeID']);						
				$empNominations[$key]['FullName'] = utf8_encode($nomination_array[$key]['FullName']);
				$empNominations[$key]['RM1FullName'] = utf8_encode($nomination_array[$key]['RM1FullName']);
				$empNominations[$key]['RM2FullName'] = utf8_encode($nomination_array[$key]['RM2FullName']);		
				$empNominations[$key]['RaisedBy'] = utf8_encode($nomination_array[$key]['RaisedBy']);
				$empNominations[$key]['Team'] = utf8_encode($nomination_array[$key]['Team']);
				$empNominations[$key]['Division'] = utf8_encode($nomination_array[$key]['Division']);
				$empNominations[$key]['ContentProvider'] = utf8_encode($nomination_array[$key]['ContentProvider']);
				$empNominations[$key]['CourseName'] = utf8_encode($nomination_array[$key]['CourseName']);
				$empNominations[$key]['CourseCosting'] = utf8_encode($nomination_array[$key]['CourseCosting']) ? utf8_encode($nomination_array[$key]['CourseCosting']) : 0;
				$empNominations[$key]['CourseDuration'] = utf8_encode($nomination_array[$key]['CourseDuration']);
				$empNominations[$key]['RaisedOn'] = utf8_encode($nomination_array[$key]['RaisedOn']);
				$empNominations[$key]['NominationRequestID'] = utf8_encode($nomination_array[$key]['NominationRequestID']);

				if($nomination_array[$key]['ApproveStatus'] == 5) {
					$classname = 'uk-badge-info';
				}
				else if($nomination_array[$key]['ApproveStatus'] == 4) {
					$classname = 'uk-badge-warning';
				}
				else if($nomination_array[$key]['ApproveStatus'] == 2) {
					$classname = 'uk-badge-success';
				}
				else if($nomination_array[$key]['ApproveStatus'] == 3) {
					$classname = 'uk-badge-danger';
				}
				else {
					$classname = 'uk-badge-outline';
				}
				
				$empNominations[$key]['Status'] = "<span class='uk-badge ".$classname." uk-text-upper'>".utf8_encode($nomination_array[$key]['StatusName'])."</span>";
				
				$action = "<a type='button' ng-click='viewNominationInfo(\"".$nomination_array[$key]['NominationID']."\",\"" .$nomination_array[$key]['EmployeePID']. "\")' class='md-btn md-btn-danger md-btn-small'>View</a>";
				
				if($nomination_array[$key]['ApproveStatus'] == 1 && $PID == $empNominations[$key]['EmployeePID']) {
					$action = $action . "<a type='button' ng-click='cancelNomination(\"".$nomination_array[$key]['NominationID']."\")' class='md-btn md-btn-danger md-btn-small'>Cancel</a>";
				}

				$empNominations[$key]['Action'] = $action;
			}
			$response['empNominations'] = $empNominations;
			$response['total'] = $response_n['total'];
							
		}
		else {
			$response['empNominations'] = array();
			$response['total'] = 0;
		}				
		echo json_encode($response);
	}

	public function getAccessStatus() {
		if ($this->session->userdata('EmployeeID')) {
			$response = $this->Dashboard_Model->getAccessStatus();
		}
		else {
			$response['error'] = 'Session expired. Please login to proceed';
		}
		echo json_encode($response);
	}

	public function updateNominationDetail() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);

		$nominationID = isset($request_pay_load['nominationID']) ? $request_pay_load['nominationID'] : '';
		$commentsBy = isset($request_pay_load['commentsBy']) ? $request_pay_load['commentsBy'] : '';
		$comments = isset($request_pay_load['comments']) ? $request_pay_load['comments'] : '';
		$acceptStatus = isset($request_pay_load['acceptStatus']) ? $request_pay_load['acceptStatus'] : '';
		$training_type = isset($request_pay_load['training_type']) ? $request_pay_load['training_type'] : '';
		$dhPID = isset($request_pay_load['dhPID']) ? $request_pay_load['dhPID'] : '';

		$additionalCost = isset($request_pay_load['additionalCost']) ? $request_pay_load['additionalCost'] : '';

		if($nominationID != '' && $commentsBy != '') {
			$response = $this->Dashboard_Model->updateNominationDetail($nominationID, $commentsBy, $comments, $additionalCost, $acceptStatus, $training_type, $dhPID);
		}
		else {
			$response['error'] = 'Could not process the request. Mandatory data missing';			
		}
		echo json_encode($response);


	}

	public function uploadFileReceipt() {
		$requestPayload = $this->input->post();
		$fileType = $requestPayload['fileType'];
		$nominationID = $requestPayload['nominationID'];
		if($requestPayload['fileType'] != '') {
			$empNo = $this->session->userdata('EmployeeID');
			$emPID = $this->session->userdata('PID');
			if(!empty($_FILES)){
				$filesCount = count($_FILES['file']['name']);
				
				$uploadfolder = './receipts/'.$empNo;

				if (!is_dir($uploadfolder)) {
					mkdir($uploadfolder, 0700);				
				}

				if(!move_uploaded_file($_FILES['file']['tmp_name'],"$uploadfolder/".$_FILES['file']['name'])) {	
					$response['error'] = 'File not uploaded. Please upload again';
				}
				else {
					$uploadData['file_name'] = $_FILES['file']['name'];
					$uploadData['uploaded_on'] = date("Y-m-d H:i:s");
					$response = $this->Dashboard_Model->uploadFileReceipt($fileType, $nominationID, $emPID, $uploadData['file_name'], $emPID);
				}
			}
			else {
				$response['error'] = 'File not selected properly. Please upload again';
			}
		}
		else {
			$response['error'] = 'File type not specified. Please upload again';
		}		
		echo json_encode($response);
	}

	public function getApprovedCourses() {
		if ($this->session->userdata('EmployeeID')) {
			$response = $this->Dashboard_Model->getApprovedCourses();
		}
		else {
			$response['error'] = 'Session expired. Please login to proceed';
		}
		echo json_encode($response);
	}

	public function getDocuments() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);
		$start = isset($request_pay_load['start']) ? $request_pay_load['start'] : '';
		$limit = isset($request_pay_load['limit']) ? $request_pay_load['limit'] : '';
		$search = isset($request_pay_load['search']['value']) ? $request_pay_load['search']['value'] : '';
		$regex = isset($request_pay_load['search']['regex']) ? $request_pay_load['search']['regex'] : false;
		$isadmin = isset($request_pay_load['isadmin']) ? $request_pay_load['isadmin'] : '';
		$ishr = isset($request_pay_load['ishr']) ? $request_pay_load['ishr'] : '';			
		$response = array();
		
		$response_n = $this->Dashboard_Model->getDocuments($start, $limit, $search, $regex);
		
		if (isset($response_n['documents']) && !empty($response_n['documents']))
		{
			$inddocuments = $response_n['documents'];

			$documents_array = array_values($inddocuments);			

			foreach($documents_array as $key => $document)
			{
				$empDocuments[$key]['DocumentId'] = utf8_encode($documents_array[$key]['DocumentId']);
				$empDocuments[$key]['EmployeePID'] = utf8_encode($documents_array[$key]['EmployeePID']);
				$empDocuments[$key]['DocumentName'] = utf8_encode($documents_array[$key]['DocumentName']);						
				$empDocuments[$key]['ContentProviderName'] = utf8_encode($documents_array[$key]['ContentProviderName']);
				$empDocuments[$key]['CourseName'] = utf8_encode($documents_array[$key]['CourseName']);
				$empDocuments[$key]['CourseCosting'] = utf8_encode($documents_array[$key]['CourseCosting']);		
				$empDocuments[$key]['CourseDuration'] = utf8_encode($documents_array[$key]['CourseDuration']);
				$empDocuments[$key]['ApproveStatus'] = utf8_encode($documents_array[$key]['ApproveStatus']);
				$empDocuments[$key]['StatusName'] = utf8_encode($documents_array[$key]['StatusName']);
				$empDocuments[$key]['UploadedOn'] = date('Y-m-d', strtotime(utf8_encode($documents_array[$key]['UploadedOn'])));

				if($documents_array[$key]['ApproveStatus'] == 3) {
					$classname = 'uk-badge-info';
				}
				else if($documents_array[$key]['ApproveStatus'] == 4) {
					$classname = 'uk-badge-warning';
				}
				else if($documents_array[$key]['ApproveStatus'] == 5) {
					$classname = 'uk-badge-success';
				}
				else if($documents_array[$key]['ApproveStatus'] == 6) {
					$classname = 'uk-badge-danger';
				}
				else {
					$classname = 'uk-badge-outline';
				}
							
				$empDocuments[$key]['Status'] = "<span class='uk-badge ".$classname." uk-text-upper'>".utf8_encode($documents_array[$key]['StatusName'])."</span>";
				$empDocuments[$key]['Action'] = "<a type='button' ng-click='viewDocumentInfo(\"".$documents_array[$key]['DocumentId']."\",\"" .$documents_array[$key]['EmployeePID']. "\")' class='md-btn md-btn-danger md-btn-small'>View</a>";
			}
			$response['empDocuments'] = $empDocuments;
			$response['total'] = $response_n['total'];
							
		}
		else {
			$response['empDocuments'] = array();
			$response['total'] = 0;
		}				
		echo json_encode($response);
	}

	public function getNominatedDocument() {
		$request_pay_load = json_decode(file_get_contents('php://input'), true);
				
		if($request_pay_load != '') {
			$documentID = isset($request_pay_load['documentID']) ? $request_pay_load['documentID'] : '';
			$empPID = isset($request_pay_load['empPID']) ? $request_pay_load['empPID'] : '';
			$response = $this->Dashboard_Model->getNominatedDocument($documentID, $empPID);
		}
		else {
			$response['error'] = 'Could not process the request. Mandatory data missing';			
		}
		echo json_encode($response);
	}

	public function getdashboardList() {
		if ($this->session->userdata('EmployeeID')) {
			$response = $this->Dashboard_Model->getdashboardList();
		}
		else {
			$response['error'] = 'Session expired. Please login to proceed';
		}
		echo json_encode($response);
	}

	function getAccessType(){
		$result = $this->Dashboard_Model->getAccessType();

		if(count($result) > 0){
			echo json_encode($result[0]);
		}
	}

	function getContentProviders() {
		$result = $this->Dashboard_Model->getContentProviders();

		echo json_encode($result);
	}
    
	function getNominations() {
		$_POST = $this->input->get();
		
		$OptionalCondition = "";
		if (empty($_POST))
		{
			$fromDate = Date('Ymd',strtotime('-1 month'));
			$toDate = Date('Ymd');
			$OptionalCondition .= " AND (CAST(N.raisedOn AS DATE) BETWEEN ''$fromDate'' AND ''$toDate'')";
			$result = $this->Dashboard_Model->getNominations($OptionalCondition);
			if (count($result)>0)
			{
				echo json_encode($result);
			}
		}else
		{
			$FormRules = array(
				array(
					'field' => 'fromDate',
					'label' => 'From date',
					'rules' => 'required|exact_length[10]|callback_checkValidDate'
				),
				array(
					'field' => 'toDate',
					'label' => 'To date',
					'rules' => 'required|exact_length[10]|callback_checkValidDate'
				)
			);
			
			$this->form_validation->set_data($this->input->get());
			$this->form_validation->set_rules($FormRules);
			
			if ($this->form_validation->run() == TRUE)
			{
				$FromDate = Date('Ymd',strtotime($_POST['fromDate']));
				$toDate = Date('Ymd',strtotime($_POST['toDate']));
				

				$OptionalCondition .= " AND (CAST(N.raisedOn AS DATE) BETWEEN ''$FromDate'' AND ''$toDate'')";
				$result = $this->Dashboard_Model->getNominations($OptionalCondition);
				if (count($result)>0)
				{
					echo json_encode($result);
				}
			}
			
		}
		
	}

	function applyCourse() {
		$formData = json_decode(file_get_contents("php://input"), true);
		$formData['formData']['PID'] = isset($formData['formData']['PID']) ? $formData['formData']['PID'] : $this->session->userdata('PID');

		$formData['formData']['name'] = str_replace("'", "''", $formData['formData']['name']);
		$formData['formData']['url'] = str_replace("'", "''", $formData['formData']['url']);
		$formData['formData']['duration'] = str_replace("'", "''", $formData['formData']['duration']);
		$formData['formData']['reason'] = str_replace("'", "''", $formData['formData']['reason']);

		$xml = new SimpleXMLElement('<courseInformation/>');
		array2XML($xml, $formData['formData']);
		$courseInformation = $xml->asXML();

		$result = $this->Dashboard_Model->applyCourse($courseInformation);

		if(count($result) > 0) {
			echo json_encode($result[0]);
		}
	}

	

	/*
	function cancelNomination() {
		$_POST = json_decode(file_get_contents("php://input"), true);

		$nominationId = $_POST['nominationId'];
		$comments = $_POST['comments'];

		$result = $this->Dashboard_Model->cancelNomination($nominationId, $comments);

		if(count($result > 0))
			echo json_encode($result[0]);
	}
	*/

	function getNomination($nominationId = NULL){

		// $_GET = $this->input->get();

		// $nominationID = $_GET['nominationId'];
		
		$result = $this->Dashboard_Model->getNomination($nominationId)[0];

		$result->timeline = $this->Dashboard_Model->getNominationTimeline($nominationId);

		$result->installments = $this->Dashboard_Model->getInstalments($nominationId);

		$installmentsForTooltip = "";
		for($i = 0; $i < count($result->installments); $i++){
			$installmentsForTooltip .= $result->installments[$i]->amount .' on '. strtolower($result->installments[$i]->actualProcessingDate) .'<br/>';
		}
		$result->installmentsForTooltip = $installmentsForTooltip;

		echo json_encode($result);

	}

	function updateNomination(){
		$_POST = json_decode(file_get_contents("php://input"), true);

		$nominationId = $_POST['nominationId'];

		$_POST['courseName'] = str_replace("'", "''", $_POST['courseName']);
		$_POST['courseUrl'] = str_replace("'", "''", $_POST['courseUrl']);
		$_POST['duration'] = str_replace("'", "''", $_POST['duration']);
		$_POST['comments'] = str_replace("'", "''", $_POST['comments']);

		$_POST['startDate'] = date('Y-m-d', strtotime($_POST['startDate']));
		$_POST['endDate'] = date('Y-m-d', strtotime($_POST['endDate']));

		$xml = new SimpleXMLElement('<nomination/>');
		array2XML($xml, $_POST);
		$nomination = $xml->asXML();

		$result = $this->Dashboard_Model->updateNomination($nomination);

		$this->getNomination($nominationId);
	}

	function updateNominationStatus(){
		$_POST = json_decode(file_get_contents("php://input"), true);
		
		$nominationId = $_POST['nominationId'];
		$comments = $_POST['comments'];
		$status = $_POST['status'];

		$result = $this->Dashboard_Model->updateNominationStatus($nominationId, $comments, $status);
		if(count($result > 0)){
			echo json_encode($result[0]);
		}
	}

	function getInstalments() {
		$_GET = $this->input->get();

		$nominationID = $_GET['nominationId'];
		
		$result = $this->Dashboard_Model->getInstalments($nominationID);
		echo json_encode($result);
	}

	function getInstallmentStatus() {
		$result = $this->Dashboard_Model->getInstallmentStatus();
		echo json_encode($result);
	}

	function updateInstallment() {
		$_POST = json_decode(file_get_contents("php://input"), true);
		
		$id = $_POST['id'];
		$amount = $_POST['amount'];
		$result = $this->Dashboard_Model->updateInstallment($id, $amount);
		
	}

	function updateInstallmentStatus() {
		$_POST = json_decode(file_get_contents("php://input"), true);
		
		$id = $_POST['id'];
		$statusId = $_POST['statusId'];
		$result = $this->Dashboard_Model->updateInstallmentStatus($id, $statusId);
		
		if(count($result) > 0){
			echo json_encode($result[0]);
		}
	}

	function updateInstallmentReceiptStatus() {
		$_POST = json_decode(file_get_contents("php://input"), true);
		
		$id = $_POST['id'];
		$statusId = $_POST['statusId'];
		$result = $this->Dashboard_Model->updateInstallmentReceiptStatus($id, $statusId);

		if(count($result) > 0){
			echo json_encode($result[0]);
		}
		
	}

	function updateInstallments() {
		$_POST = json_decode(file_get_contents("php://input"), true);

		$xml = new SimpleXMLElement('<instalments/>');
		array2XML($xml, $_POST);
		$installments = $xml->asXML();

		$result = $this->Dashboard_Model->updateInstallments($installments);
		echo json_encode($result);
	}

	function updateInstallmentReceipt() {
		
		$_POST = $this->input->post();
		
		$installmentId = $_POST['installmentId'];
		
		$splitName = explode('.', $_FILES['file']['name']);
		$fileName = md5($installmentId);
		$fileType = end($splitName);
		
		$file = $fileName .'.'. $fileType;


		if(move_uploaded_file($_FILES['file']['tmp_name'], UPLOAD_PATH_RECEIPTS . $file)) {
			$result = $this->Dashboard_Model->updateInstallmentReceipt($installmentId, $file);
			if(count($result) > 0) {
				echo json_encode($result[0]);
			}
		}
	}
	function uploadCertificate(){
		$_POST = $this->input->post();
		$splitName = explode('.', $_FILES['file']['name']);
		$fileName = md5($_POST['nominationId']);
		$fileType = end($splitName);
		
		$file = $fileName .'.'. $fileType;
		
		if(move_uploaded_file($_FILES['file']['tmp_name'], UPLOAD_PATH_CERTIFICATES . $file)) {
			$result = $this->Dashboard_Model->uploadCertificate($_POST, $file);
			if(count($result) > 0) {
				echo json_encode($result[0]);
			}
		}

	}

	function viewFile($fileName = NULL,$path = NULL){
		if($path == 'certificate'){
			$path = UPLOAD_PATH_CERTIFICATES;
		}
		else if($path == 'receipt'){
			$path = UPLOAD_PATH_RECEIPTS;
		}
		$response = file_get_contents($path. $fileName);
		header('Content-Type: application/pdf');
		echo $response;
	}

	
	function getEligibleForNomination(){
		$result = $this->Dashboard_Model->getEligibleForNomination();
		if(count($result) > 0){
			echo json_encode($result[0]);
		}
	}

	function getNominationsCount() {
		$result = $this->Dashboard_Model->getNominationsCount();

		if($result) {
			
			$nominationCounts = array();
			$total = 0;

			for ($i = 0; $i < count($result); $i++) {
				$total += $result[$i]->count;
				$nominationCounts['counts'][] = [$result[$i]->status, $result[$i]->count];
			}
			$nominationCounts['total'] = $total;

			echo json_encode($nominationCounts);

		}
	}
	/* chart functionalities starts --> karthik */
	function getContentProvidersCount(){
		$result = $this->Dashboard_Model->getContentProvidersCount();

		if($result) {
			
			$ContentProvidersCount = array();
			$total = 0;

			for ($i = 0; $i < count($result); $i++) {
				$total += $result[$i]->count;
				$ContentProvidersCount['counts'][] = [$result[$i]->status, $result[$i]->count];
			}
			$ContentProvidersCount['total'] = $total;
			echo json_encode($ContentProvidersCount);

		}
	}
	function getDivisionWiseNominationCount(){
		$result = $this->Dashboard_Model->getDivisionWiseNominationCount();
		if($result) {
			
			$nominationDivisionCounts = array();
			$keys = array_keys($result[0]);
			for($i = 0; $i < count($keys); $i++)
			{
				if($i == 0)
				{
					$nominationDivisionCounts['division_heads'] = array_column($result, $keys[$i]);
				}
				else
				{
					$column_array1 = array($keys[$i]);
					$column_array2 = array_column($result, $keys[$i]);
					$nominationDivisionCounts['total_and_nominated'][] = array_merge_recursive($column_array1, $column_array2);
				}
			}
			echo json_encode($nominationDivisionCounts);

		}

	}
	function getExitEmpList(){
		$result = $this->Dashboard_Model->getExitEmpList();
		if($result) {
			echo json_encode($result);
		}
	}
	function getBudgetReport(){
		$_POST = $this->input->get();
		$Year = $_POST['Year'];
		$result = $this->Dashboard_Model->getDivisionWiseBudget($Year);
		if($result) {
			$getBudgetReport = array();
			foreach($result as $key => $value){
				if($value['Utilized'] == '')
				{
					$value['Utilized'] = 0;
				}
				if($value['Balance'] == '')
				{
					$value['Balance'] = 0;
				}
				$getBudgetReport[] = $value;
			}
			echo json_encode($getBudgetReport);
		}
	}
	function getDivisionWiseHeadsBudget(){
		$result = $this->Dashboard_Model->getDivisionHeadsBudget();
		if($result) {
			$getDivisionWiseHeadsBudget = array();
			$keys = array_keys($result[0]);
			for($i = 0; $i < count($keys); $i++)
			{
				if($i == 0)
				{
					$getDivisionWiseHeadsBudget['division_heads'] = array_column($result, $keys[$i]);
				}
				else if($i != 1 && $i != 2 && $i != 9 && $i != 8 && $i != 7 && $i != 6)
				{
					$column_array1 = array($keys[$i]);
					$column_array2 = array_map(function($value) {
										return $value == "" ? 0 : $value;
									 }, array_column($result, $keys[$i]));
					$getDivisionWiseHeadsBudget['divisionBudget'][] = array_merge_recursive($column_array1, $column_array2);
				}
			}
			echo json_encode($getDivisionWiseHeadsBudget);
		}
	}
	function getDivisionWiseNominationStatistics(){
		$result = $this->Dashboard_Model->getDivisionWiseNominationStatistics();
		if($result) {
			$getDivisionWiseNominationStatistics = array();
			$keys = array_keys($result[0]);
			for($i = 0; $i < count($keys); $i++)
			{
				if($i == 0)
				{
					$getDivisionWiseNominationStatistics['division_heads'] = array_column($result, $keys[$i]);
				}
				else
				{
					$column_array1 = array($keys[$i]);
					$column_array2 = array_column($result, $keys[$i]);
					$getDivisionWiseNominationStatistics['statistics'][] = array_merge_recursive($column_array1, $column_array2);
				}
			}
			echo json_encode($getDivisionWiseNominationStatistics);
		}
	}
	/* chart functionalities ends --> karthik */
	function getDivisionHeadBudget() {
		$result = $this->Dashboard_Model->getDivisionHeadBudget();
		
		if($result) {
			
			$budget = array();
			$statistics = array();
			
			for ($i = 0; $i < count($result); $i++) {
				$budget['Consumes'] = [
					["Utilized", $result[$i]['Utilized']],
					["Balance", $result[$i]['Balance'] ? $result[$i]['Balance']: $result[$i]['Allocated']]
				];
				$budget['Allocated'] = $result[$i]['Allocated'];
				$budget['FullName'] = $result[$i]['Employee Name'];
				$budget['PID'] = $result[$i]['PID'];

				array_push($statistics, $budget);
			}
			echo json_encode($statistics[0]);
			
			
		}
	}
	/********* Crew Nomination Starts *********/
	/*function getCrew(){
		$PID = $this->session->userdata('PID');
		$result = $this->Dashboard_Model->getCrew($PID);
		echo json_encode($result);
	}*/

	/********* Crew Nomination Ends *********/
}
