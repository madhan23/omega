<?php
	$GLOBALS['key'] = "M3R!t2016#";  // Encryption Key	
	// Encrypt the string
	function encrypt($string) {
	   $result = "";
        for($i=0; $i<strlen($string); $i++){
                $char = substr($string, $i, 1);
                $keychar = substr($GLOBALS['key'], ($i % strlen($GLOBALS['key']))-1, 1);
                $char = chr(ord($char)+ord($keychar));
                $result.=$char;
        }
        $salt_string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxys0123456789";
        $length = rand(1, 15);
        $salt = "";
        for($i=0; $i<=$length; $i++){
                $salt .= substr($salt_string, rand(0, strlen($salt_string)), 1);
        }
        $salt_length = strlen($salt);
        $end_length = strlen(strval($salt_length));
        //return base64_encode($result.$salt.$salt_length.$end_length);
		return strtr(base64_encode($result.$salt.$salt_length.$end_length), '/', '_');
	}
	
	// 	For decrypt encrypted string
	function decrypt($string) {
		$string = strtr($string,'_','/');
		$result = "";
        $string = base64_decode($string);
        $end_length = intval(substr($string, -1, 1));
        $string = substr($string, 0, -1);
        $salt_length = intval(substr($string, $end_length*-1, $end_length));
        $string = substr($string, 0, $end_length*-1+$salt_length*-1);
        for($i=0; $i<strlen($string); $i++){
                $char = substr($string, $i, 1);
                $keychar = substr($GLOBALS['key'], ($i % strlen($GLOBALS['key']))-1, 1);
                $char = chr(ord($char)-ord($keychar));
                $result.=$char;
        }
        return $result;
	}
	
	// Convert date with ago format
	function get_timeago($ptime) {		
		$now = new DateTime;  // Current Time
		$ago = new DateTime($ptime);  // Start date
		$diff = $now->diff($ago); // Different between 2 days

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		$string = array_slice($string, 0, 2);
		return implode(' and ', $string);
	}

	function array2XML($obj, $array)
	{
		foreach ($array as $key => $value)
		{
			if(is_numeric($key))
				$key = 'data';
			if (is_array($value))
			{
				$node = $obj->addChild($key);
				//$this->array2XML($node, $value);
				array2XML($node, $value);
			}
			else
			{
				$obj->addChild($key, htmlspecialchars($value));
			}
		}
	}
?>
