<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('array_to_csv'))
{
    function array_to_csv($array, $download = "")
    {
        if ($download != "")
        {    
			header('Content-Type: application/csv');
            header('Content-Disposition: attachement; filename="' . $download . '"');
        }        

        ob_start();
        $f = fopen('php://output', 'w') or show_error("Can't open php://output");
        $n = 0;    
        foreach ($array as $line)
        {
            $n++;
            if ( ! fputcsv($f, $line))
            {
                show_error("Can't write line $n: $line");
            } 
        }
        fclose($f) or show_error("Can't close php://output");
        $str = ob_get_contents();
        ob_end_clean();

        if ($download == "")
        {
            return $str;    
        }
        else
        {    
            echo $str;
        }        
    }
}

if(!function_exists('query_to_csv2')) {
	function query_to_csv2($query, $headers = TRUE, $download = "") {
		$counter=0;$keyCnt=array();$array = array();
		foreach($query->result_array() as $row) {	
			if($counter==0) {
				$line = array();
				foreach($row as $key=>$value) {
					$line[] = $key;	            
				}				
				$array[] = $line;
			}

			$line = array();
			foreach($row as $key=>$value) {
				$line[] = $value;				 	
			}				
			$array[] = $line;
			$counter++;
	    }	
        echo array_to_csv($array, $download);
	}
} 

if ( ! function_exists('query_to_csv'))
{
    function query_to_csv($query, $headers = TRUE, $download = "")
    {
       	/* $counter=0;$keyCnt=array();$array = array();
		foreach($query->result_array() as $row) {	
			if($counter==0) {
				$line = array();
				foreach($row as $key=>$value) {
					$line[] = $key;	            
				}				
				$array[] = $line;
			}

			$line = array();
			foreach($row as $key=>$value) {
				$line[] = $value;				 	
			}				
			$array[] = $line;
			$counter++;
	    }	*/
		
		$data = $query->result_array();
		$array[] = $head = array_keys($data[0]);
		$line = array();
		$line[] = "";
		for($i=1; $i<count($head); $i++) {
			$line[] = "LH,RH,RU";
		}			
		$array[] = $line;
		//print_r($array); exit;
		/* $arr= array();
		$headers = array_keys($data[0]);
		$totResource=0;
		$totExcess=0;
		//$arr["head"] = $headers;
		foreach($data as $k=>$v) {
			$arr[$k][$headers[0]] = $v[$headers[0]];
			$rcnt=0;
			$ecnt=0;
			for($i=1; $i<count($headers); $i++) {
				$res[$i] = $v[$headers[$i]] / 60;
				//$arr[$k][$headers[$i]] = floor($v[$headers[$i]] / 60);
				$arr[$k]['LH'] = floor($v[$headers[$i]] / 60);
				$arr[$k]['RH'] = $ec = floor($v[$headers[$i]] / 60) > 8?floor($v[$headers[$i]] / 60)-8:0;
				$arr[$k]['RU'] = $rc = floor($v[$headers[$i]] / 60) < 8?floor($v[$headers[$i]] / 60) / 8:1;
				$rcnt += $rc;
				$ecnt += $ec;
			}			
			$arr[$k]['Resource'] = $rcnt;
			$arr[$k]['Excess'] = $ecnt;
			$totResource +=$rcnt;
			$totExcess +=$ecnt;
		}
		// echo "\nLast Row:".$lastRow;
		// echo "\nLast Row:".$totResource;
		// echo "\nLast Row:".$totExcess;
		$k = $k+1;		
		$arr[$k][$headers[0]] = "&nbsp;";
		for($i=1; $i<count($headers); $i++) {
			$arr[$k]['LH<span style="display:none">-'.$i."</span>"] = "&nbsp;";
			$arr[$k]['RH<span style="display:none">-'.$i."</span>"] = "&nbsp;";
			$arr[$k]['RU<span style="display:none">-'.$i."</span>"] = "&nbsp;";
		}
		$arr[$k]['Resource'] = $totResource;
		$arr[$k]['Excess'] = $totExcess; */

        echo array_to_csv($array, $download);
    }
}

/* End of file csv_helper.php */
/* Location: ./system/helpers/csv_helper.php */