angular
    .module('meritOmegaApp')
    .controller('nominateEmployeeCtrl', [
        '$scope',
        '$rootScope',
        '$http',
		'$filter',
		'$compile',
		'DTOptionsBuilder',
		'DTColumnBuilder',
		'employeeDeps',
		'$location',
		'utils',
        function ($scope,$rootScope,$http,$filter,$compile,DTOptionsBuilder,DTColumnBuilder,employeeDeps,$location,utils) {

			$scope.nominations = {};
			$scope.disableOption = false;
            $scope.enableInstallments = false;
            $scope.enableOptionDetail = false;

			if (!employeeDeps || employeeDeps.data.error) {
            	var error = employeeDeps.data.error ? employeeDeps.data.error : employeeDeps.statusText;
            	UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+error+'</div>');
            } else {
				
				var teamList = [];				
				teamList = employeeDeps.data.teamList;
				var content_provider = employeeDeps.data.content_provider;
				var contentPro = [];

				var payment_types = [
                    { 'id' : 'Single', 'value' : 'Single' },
                    { 'id' : 'Weekly', 'value' : 'Weekly' },
                    { 'id' : 'Fortnightly', 'value' : 'Fortnightly' },
                    { 'id' : 'Monthly', 'value' : 'Monthly' },
                    { 'id' : 'Quarterly', 'value' : 'Quarterly' },
                    { 'id' : 'Others', 'value' : 'Others' },                    
                ];

				$scope.k_employee_list = {
					dataSource: teamList,
                    dataTextField: "FullName",
					dataValueField: "PID",
					optionLabel: 'Select an Employee'
				};

				$scope.k_reporting_manager_list = {					
                    optionLabel: 'Reporting Manager'
				};
				
				angular.forEach(content_provider, function(content) {
					contentPro.push({
						'CPID':content['CPID'],
						'CPName': content['ContentProviderName']
					});
				});

				$scope.k_content_provider_list = {
					dataSource: contentPro,
					dataTextField: "CPName",
					dataValueField: "CPID",				
					optionLabel: 'Select the Content Provider'
				};

				$scope.k_payment_type_list = {
					dataSource: payment_types,
                    dataTextField: 'id',
                    dataValueField: 'value'
				};

			}

			$scope.disableOptions = function(value) {
				if(value == 'Free') {
					$scope.disableOption = true;
				} else {
					$scope.disableOption = false;
				}
			}

			$scope.check_installments = function() {
				if($scope.nominations.course.payment_type != 'Single') {
					$scope.enableInstallments = true;
					if($scope.nominations.course.payment_type == 'Others') {
						$scope.enableOptionDetail = true;
					} else {
						$scope.enableOptionDetail = false;
					}
				} else {
					$scope.enableInstallments = false;
					$scope.enableOptionDetail = false;
				}
			}

			$scope.getEmployeeDetails = function(){
				$http({
					url: 'dashboard/getEmpHeadDetails',
					method: 'POST',
					data: {
						empPID: $scope.nominations.employee.employeePID
					}
				}).then(function successCallback(response){
					if (!response || response.data.error) {
						var error = (response.data.error? response.data.error: response.statusText);
						UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+error+'</div>');
					}
					else {
						var empHeadDet = response.data.empHeadDet;
						var repManagers = [
							{
								'RMPID': empHeadDet['RM1PID'],
								'RMName': empHeadDet['RM1Fname']
							},
							{
								'RMPID': empHeadDet['RM2PID'],
								'RMName': empHeadDet['RM2Fname']
							}
						];
						
						$scope.k_reporting_manager_list = {
							dataSource: repManagers,
							dataTextField: "RMName",
							dataValueField: "RMPID",				
							optionLabel: 'Reporting Manager'
						}

						$scope.nominations.employee = {
							'employeePID': empHeadDet['PID'],
							'employeeID': empHeadDet['EmployeeID'],
							'team': empHeadDet['TeamName'],
							'Division': empHeadDet['Division'],
							'ReportingManager': empHeadDet['RM1PID'],
							'ReportingManagerID': empHeadDet['RM1EmpID'],
							'DivisionHead': empHeadDet['DivHeadFName'],
							'DivisionHeadID': empHeadDet['DivHEmpID'],
							'EmpPID': empHeadDet['PID'],
							'RM1PID': empHeadDet['RM1PID'],
							'DHPID': empHeadDet['DivHPID'],
							'TeamID': empHeadDet['TeamID'],
							'DivisionID': empHeadDet['DivisionID'],
						}
					}
				}, function failureCallback(response){
					var error = (response.data.error ? response.data.error : response.statusText);                        
					UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+response.statusText+'</div>');                        
				});
			}

			$scope.clearNominationForm = function() {
				$scope.nominations = {};
			}

			$scope.nominateEmployee = function() {
				$('#emp_nomination_form').parsley().validate();
                if ($('#emp_nomination_form').parsley().isValid()) {
					$scope.nominations.installments = $scope.installments;
					$http({
                        url: 'dashboard/nominateEmp',
                        method: 'POST',
                        data: {
	                        formData: $scope.nominations
	                    }
                    }).then(function successCallback(response){
                        if (!response || response.data.error) {
                            var error = (response.data.error ? response.data.error : response.statusText);
                            UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+error+'</div>');
                        } else {
                            $('#emp_nomination_form').parsley().reset();
                            UIkit.modal.alert('<div class=\'uk-text-center uk-text-success\'>OMEGA nomination has been Submitted.</div>');
                            $location.path( "/nomination" );
                        }
                    }, function failureCallback(response){
                        var error = (response.data.error ? response.data.error : response.statusText);                        
                        UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+response.statusText+'</div>');                        
                    });
				}
				else {
					UIkit.modal.alert('Please provide all required details.');
				}
			}

			$scope.instalmentRange = function(count) {
				count = parseInt(count);
				$scope.installments = [];

				for(var i=0; i<count; i++) {
					$scope.installments.push({amount: '', processDate: ''});
				}
			}

			$scope.monthSelectorOptions = {
				min: new Date(),
				start: "year",
                depth: "year"
            };
        }
    ]);  
