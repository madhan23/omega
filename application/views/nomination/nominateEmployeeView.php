<div id="page_content">
    <div id="page_content_inner">
        <div class="md-card uk-width-1 uk-container-center">
            <div class="md-card-content large-padding">
                <form id="emp_nomination_form" class="uk-form-stacked" novalidate>                    
                    <div class="custom-form">
                        
                        <div class="heading_a">
                            Employee Details
                        </div>

                        <!-- Employee Details -->
                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-grid-width-large-1-4" data-uk-grid-margin>
                            <div class="parsley-row">
                                <label for="employeeName" class="uk-form-label">Employee Name<span class="req">*</span></label>
                                <select kendo-drop-down-list
                                id="employee" 
                                k-options="k_employee_list" 
                                k-rebind="k_employee_list" 
                                ng-model="nominations.employee.employeePID" 
                                k-on-change="getEmployeeDetails()"
                                required></select>
                            </div>

                            <div class="parsley-row">
                                <label>&nbsp;</label>
                                <label for="employeeID">Employee ID<span class="req">*</span></label>
                                <input 
                                    type="text" 
                                    id="EmployeeID" 
                                    class="md-input" 
                                    maxlength="100" 
                                    ng-model="nominations.employee.employeeID"                                         
                                    ng-disabled="true"
                                    required 
                                    md-input />
                            </div>

                            <div>
                                <div class="parsley-row">
                                    <label>&nbsp;</label>
                                    <label for="employeeID">Team<span class="req">*</span></label>
                                    <input 
                                        type="text" 
                                        id="EmployeeID" 
                                        class="md-input" 
                                        maxlength="100" 
                                        ng-model="nominations.employee.team"
                                        ng-disabled="true"
                                        required 
                                        md-input />
                                </div>
                            </div>
                            <div>
                                <div class="parsley-row">
                                <label>&nbsp;</label>
                                <label for="Division">Division<span class="req">*</span></label>
                                <input 
                                    type="text" 
                                    id="EmployeeID" 
                                    class="md-input" 
                                    maxlength="100" 
                                    ng-model="nominations.employee.Division"
                                    ng-disabled="true"
                                    required 
                                    md-input />
                                </div>
                            </div>
                        </div>
                        <!-- Employee Details -->

                        <!-- Manager / Division Head Details -->
                        
                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-grid-width-large-1-4" data-uk-grid-margin>
                            <div class="parsley-row">
                                <label for="employeeName" class="uk-form-label">Reporting Manager Name<span class="req">*</span></label>
                                <select kendo-drop-down-list
                                id="reporting_manager" 
                                k-options="k_reporting_manager_list" 
                                k-rebind="k_reporting_manager_list" 
                                ng-model="nominations.employee.ReportingManager"
                                k-option-label="'Reporting Manager'"
                                ng-disabled="true"
                                required></select>
                            </div>

                            <div class="parsley-row">
                                <label>&nbsp;</label>
                                <label for="employeeID">Reporting Manager Emp ID<span class="req">*</span></label>
                                <input 
                                    type="text" 
                                    id="ReportingManagerID" 
                                    class="md-input" 
                                    maxlength="100" 
                                    ng-model="nominations.employee.ReportingManagerID"
                                    ng-disabled="true"
                                    required 
                                    md-input />
                            </div>

                            <div>
                                <div class="parsley-row">
                                    <label>&nbsp;</label>
                                    <label for="Division">Head of Division Name<span class="req">*</span></label>
                                    <input 
                                        type="text" 
                                        id="division" 
                                        class="md-input" 
                                        maxlength="100" 
                                        ng-model="nominations.employee.DivisionHead"
                                        ng-disabled="true"
                                        required 
                                        md-input />
                                   
                                </div>
                            </div>
                            <div>
                                <div class="parsley-row">
                                    <label>&nbsp;</label>
                                    <label for="employeeID">Head of Division Emp ID<span class="req">*</span></label>
                                    <input 
                                        type="text" 
                                        id="DivisionHeadID" 
                                        class="md-input" 
                                        maxlength="100" 
                                        ng-model="nominations.employee.DivisionHeadID"
                                        ng-disabled="true"
                                        required 
                                        md-input />
                                </div>
                            </div>
                            
                        </div>
                        <!-- Manager / Division Head Details -->
                        
                        <div class="heading_a">
                            Course Details
                        </div>

                        <!-- Course Details -->

                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>
                            <div class="parsley-row">
                                <label class="uk-form-label">Course Type<span class="req">*</span></label>
                                <span class="icheck-inline">
                                    <input type="radio" name="wizard_course_type" id="wizard_course_type" required value="Free" 
                                    ng-change='disableOptions(nominations.course.course_type)'
                                    icheck ng-model="nominations.course.course_type" />
                                    <label for="wizard_free_access" class="inline-label">Free</label>
                                </span>
                                <span class="icheck-inline">
                                    <input type="radio" name="wizard_course_type" id="wizard_to_spend_on" class="wizard-icheck" value="Paid" 
                                    ng-change='disableOptions(nominations.course.course_type)'
                                    icheck ng-model="nominations.course.course_type" />
                                    <label for="wizard_to_spend_on" class="inline-label">Paid</label>
                                </span>
                            </div>

                            <div class="parsley-row">
                                <label class="uk-form-label">Training Type<span class="req">*</span></label>
                                <span class="icheck-inline">
                                    <input type="radio" name="Online" id="Online" value="Online" icheck ng-model="nominations.course.training_type" ng-required ="!nominations.course.training_type"
                                    />
                                    <label for="wizard_free_access" class="inline-label">Online</label>
                                </span>
                                <span class="icheck-inline">
                                    <input type="radio" name="Offline" id="Offline" class="wizard-icheck" value="Offline" icheck ng-model="nominations.course.training_type"  ng-required ="!nominations.course.training_type"
                                />
                                    <label for="wizard_to_spend_on" class="inline-label">Offline</label>
                                </span>
                            </div>
                        </div>


                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>

                            <div class="parsley-row">
                                <label for="content_provider" class="uk-form-label">Content Provider Name<span class="req">*</span></label>
                                <select kendo-drop-down-list
                                style="width: 100%"
                                id="content_provider" 
                                k-options="k_content_provider_list"
                                ng-model="nominations.course.content_provider"
                                k-rebind="k_content_provider_list" 
                                required></select>
                            </div>
                            

                            <div class="parsley-row">
                                <label>&nbsp;</label>
                                <label for="course_name">Topic / Course Name<span class="req">*</span></label>
                                <input 
                                    type="text" 
                                    id="course_name" 
                                    class="md-input" 
                                    maxlength="100" 
                                    ng-model="nominations.course.course_name" 
                                    required 
                                    md-input />
                            </div>
                        </div>

                        <div class="uk-grid uk-grid-width-small-1-1 uk-grid-width-medium-1-1 uk-grid-width-large-1-1" data-uk-grid-margin>
                            <div class="parsley-row">
                                <label>&nbsp;</label>
                                <label for="course_name">Topic / Course URL<span class="req">*</span></label>
                                <input 
                                    type="text" 
                                    id="course_url" 
                                    class="md-input" 
                                    maxlength="100" 
                                    ng-model="nominations.course.course_url" 
                                    required 
                                    md-input />
                            </div>
                        </div>

                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>
                            <div>
                                <div class="parsley-row">
                                    <label>&nbsp;</label>
                                    <label for="course_cost">Total Cost (in INR) <span class="req">*</span></label>
                                    <input 
                                    class="md-input" 
                                    id="course_cost" 
									type="number"                                     
                                    ng-model="nominations.course.course_cost" 
                                    ng-required="!disableOption"
                                    ng-disabled="disableOption"
                                    style="text-align: left;">
                                </div>
                                
                            </div>
                            <div>
                                <div class="parsley-row">
                                    <label>&nbsp;</label>
                                    <label for="course_duration">Course Duration (as mentioned on website) <span class="req">*</span></label>
                                    <input 
                                    type="text" 
                                    id="course_duration" 
                                    class="md-input" 
                                    maxlength="100" 
                                    ng-model="nominations.course.course_duration" 
                                    required 
                                    md-input />
                                </div>
                            </div>
                            <div>
                                
                            </div>
                        </div>
                        <!-- Course Details -->

                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-grid-width-large-1-4" data-uk-grid-margin>
                            <div class="parsley-row">
                                <label for="employeeName" class="uk-form-label">Course Start Date (Approx)<span class="req">*</span></label>
                                <input 
                                id="course_st_dt" 
                                name="course_st_dt" 
                                ng-model="nominations.course.course_st_dt" 
                                kendo-date-picker
                                required/>
                            </div>

                            <div class="parsley-row">
                                <label for="employeeName" class="uk-form-label">Course End Date (Approx)<span class="req">*</span></label>
                                <input 
                                id="course_end_dt" 
                                name="course_end_dt" 
                                ng-model="nominations.course.course_end_dt"
                                kendo-date-picker 
                                required/>
                            </div>

                            <div class="parsley-row">
                                <label class="uk-form-label">Payment Type<span class="req">*</span></label>
                                <span class="icheck-inline">
                                    <input type="radio" name="wizard_payment_detail" id="wizard_self" 
                                    ng-required="!disableOption"
                                    ng-disabled="disableOption"
                                    value="self" icheck ng-model="nominations.course.payment_detail" />
                                    <label for="wizard_free_access" class="inline-label">Paid by self</label>
                                </span>
                                <span class="icheck-inline">
                                    <input type="radio" name="wizard_payment_detail" id="wizard_company" class="wizard-icheck" 
                                    ng-required="!disableOption && nominations.course.payment_detail != 'self'"
                                    ng-disabled="disableOption"
                                    value="company" icheck ng-model="nominations.course.payment_detail" />
                                    <label for="wizard_to_spend_on" class="inline-label">Paid by company</label>
                                </span>
                            </div>
                            <div class="parsley-row">
                                <label for="wizard_payment_type">Payment Frequency<span class="req">*</span></label>
                                <select kendo-drop-down-list
                                style="width: 100%"
                                name="wizard_payment_type"
                                id="wizard_payment_type" 
                                k-options="k_payment_type_list" 
                                k-rebind="k_payment_type_list" 
                                ng-model="nominations.course.payment_type"
                                ng-required="!disableOption"
                                ng-disabled="disableOption"
                                ng-change="check_installments()"></select>                                    
                            </div>      
                        </div>
                        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-2" data-uk-grid-margin>
                            <div class="parsley-row">

                            </div>
                            <div class="parsley-row">
                                <div style="color:#F00;" class="req" ng-show="nominations.course.payment_detail == 'self'">Kindly upload the payment receipts for processing.</div>
                                <div style="color:#F00;" class="req" ng-show="nominations.course.payment_detail == 'company'">Submit your receipts after registering for the course.</div>
                            </div>
                        </div>
                        <div class="uk-grid uk-grid-width-small-1-3 uk-grid-width-medium-1-3 uk-grid-width-large-1-3" data-uk-grid-margin ng-show="enableInstallments" >
                            
                            <div class="parsley-row" ng-show="enableOptionDetail">
                                <label for="wizard_course_end_date">Specify the payment option</label>
                                <input type="text" name="wizard_course_option_detail" id="wizard_course_option_detail" ng-required="enableInstallments && enableOptionDetail" required class="md-input" 
                                parsley-validate-input md-input ng-model="nominations.course.option_detail"/>
                            </div> 
                            <div class="parsley-row">
                                <label for="wizard_course_installment_count">Total No. of Installments<span class="req">*</span></label>
                                <input type="number" name="wizard_course_installment_count" id="wizard_course_installment_count" ng-required="enableInstallments" class="md-input" 
                                parsley-validate-input md-input ng-change="instalmentRange(nominations.course.installment_count);" ng-model="nominations.course.installment_count"/>
                                
                            </div>
                            <!--div class="parsley-row">
                                <label for="wizard_course_end_date">1st Installment Amount</label>
                                <input type="number" name="wizard_course_installment_amount" id="wizard_course_installment_amount" ng-required="enableInstallments" required class="md-input" 
                                parsley-validate-input md-input ng-model="nominations.course.installment_amount"/>
                            </div-->
						</div>
						<div class="uk-grid" ng-repeat="item in installments" ng-show="enableInstallments">
							<div class="parsley-row">
								<label for="wizard_course_end_date">{{$index + 1}}st Installment Amount</label>
								<input
									type="number"
									ng-required="enableInstallments"
									required class="md-input"
									md-input
									ng-model="item.amount"
								/>
							</div>
							<div class="parsley-row">
								<input kendo-date-picker
									k-options="monthSelectorOptions"
									k-format="'MMMM yyyy'"
									style="width: 100%;"
									ng-model="item.processDate"
									ng-required="enableInstallments"
								/>
							</div>
						</div>

                        <!-- Course Details -->

                        <div class="heading_a">
                           Reason for Nomination
                        </div>
                        <!-- Course Details -->
                        <div class="uk-grid uk-grid-width-small-1-1 uk-grid-width-medium-1-1 uk-grid-width-large-1-1" data-uk-grid-margin>
                            <div class="parsley-row">{{isDivHead}}
                                <label for="ProjectComments" ng-if="isDivHead == 1">Division Head Comments</label>
                                <label for="ProjectComments" ng-if="isDivHead == 0">Reporting Manager Comments</label>
                                <textarea 
                                class="md-input" 
                                ng-model="nominations.course.dhComments" 
                                minlength="100" 
                                data-parsley-minlength="100"
								maxlength="200" 
                                data-parsley-maxlength="200"
                                required 
                                mdInput textarea-autosize></textarea>
                            </div>                                
                        </div>
                        <!-- Course Details -->


                        <div class="uk-grid" ng-show="false">
                            <div class="uk-width-1-1 parsley-row">
                            <input type="text" name="EmpPID" id="EmpPID" class="md-input" md-input ng-model="nomination.employee.EmpPID" />
                            <input type="text" name="RM1PID" id="RM1PID" class="md-input" md-input ng-model="nomination.employee.RM1PID" />
                            <input type="text" name="DHPID" id="DHPID" class="md-input" md-input ng-model="nomination.employee.DHPID" />
                            <input type="text" name="TeamID" id="TeamID" class="md-input" md-input ng-model="nomination.employee.TeamID" />
                            <input type="text" name="DivisionID" id="DivisionID" class="md-input" md-input ng-model="nomination.employee.DivisionID" />
                            </div>                                
                        </div>
                    
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <button type="button"  style="float:right;margin-left:1%;" ng-click="clearNominationForm()" class="clsButtonUpload md-btn md-btn-primary">Clear</button>
                                <button type="submit" style="float:right;" id="nominateEmployee" ng-click="nominateEmployee()" class="md-btn md-btn-primary">Nominate</button>                                    
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>	

    </div>
</div>

<style>
	.uk-datepicker {z-index:1400!important;}
	.loader {
	    border: 5px solid #f3f3f3; /* Light grey */
	    border-top: 5px solid #3498db; /* Blue */
	    border-radius: 50%;
	    margin: 10px auto;
	    position: relative;
	    width: 30px;
	    height: 30px;
	    animation: spin 1s linear infinite;
	}

	@keyframes spin {
	    0% { transform: rotate(0deg); }
	    100% { transform: rotate(360deg); }
	}

    .custom_header {
        border-bottom: 2px solid #139b49;   
        color: #000;
        font-weight: 500;
        font-size: 18px;
        padding: 6px 0;
        margin: 0 0 25px 0;
    }
    .custom_form {
        padding: 45px 55px 65px 55px !important;        
    }
</style>
