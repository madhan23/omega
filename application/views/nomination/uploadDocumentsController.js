angular
    .module('meritOmegaApp')
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {

                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                modelSetter(scope, element[0].value);

                element.bind('change',function(){
                    scope.$apply(function(){
                        if (element[0].files) {
                            modelSetter(scope, element[0].files[0]);
                        } else {
                            modelSetter(scope, element[0].value);
                        }
                    });
                });
           }
        };
    }])
    .factory('documentService',['$resource',function($resource){
		var service = $resource('/API/V1', {}, {
			get: {
				url: 'dashboard/getDocuments',
				method: 'POST',
			}
		});
		return service;
	}])
    .factory('approvedCourseService',['$resource',function($resource){
		var service = $resource('/API/V1', {}, {
			get: {
				url: 'dashboard/getApprovedCourses',
				method: 'POST',
			}
		});
		return service;
	}])
    .controller('uploadDocumentsCtrl', [
        '$scope',
        '$rootScope',
        '$http',
		'$filter',
        '$compile',
        '$window',
		'DTOptionsBuilder',
		'DTColumnDefBuilder',
        'DTColumnBuilder',
        'approvedCourseService',
        'teamCount',
        'documentService',
		'accessStatus',
		'utils',
        function ($scope, $rootScope, $http, $filter, $compile, $window, DTOptionsBuilder, DTColumnDefBuilder, DTColumnBuilder, approvedCourseService, teamCount, documentService, accessStatus, utils) {
			
            $scope.userType = '';
            $scope.nominatedEmployees = [];
            $scope.message = "";            

            $scope.loadUploadOptions = function() {
                $scope.course_list = {};
                $scope.fileTypes = [
                    {id: '1', value: 'Payment Challan/Receipt'},
                    {id: '2', value: 'Course Certificate'}
                ];

                $scope.k_field_types = {
                    dataSource: {
                           data: $scope.fileTypes
                       },
                       dataTextField: "value",
                       dataValueField: "id",
                       optionLabel: 'Select Form Type',
                       change: function(){                	
                           this.trigger('input');
                       }
                }
                
                $scope.enableUploadOption = false;
                
                approvedCourseService.get().$promise.then(function(response){                   
                    if(response.course_list) {
                        $scope.course_list = response.course_list;
                        $scope.k_approved_course = {
                            dataSource: {
                                data: $scope.course_list
                            },
                            dataTextField: "CourseName",
                            dataValueField: "NominationID",
                            optionLabel: 'Select Course Name',
                            change: function(){                	
                                this.trigger('input');
                            }
                        }
                        $scope.enableUploadOption = true;
                    } else {
                        $scope.enableUploadOption = false;
                    }
                });               
            }

			$scope.isDivHead = 0;
			if(!teamCount || teamCount.data.error) {

			}
			else {
				var teamCount = teamCount.data.count[0].TeamCount;
				if(teamCount > 0) {
					$scope.userType = 'Team';
				} else {
					$scope.userType = 'Self';
				}
			}

			if(!accessStatus || accessStatus.data.error) {
				console.log('Error');
			} else {
				var aStatus = accessStatus.data.acessStatus;
				$scope.isDivHead = aStatus[0].IsDivHead;
            }

            $scope.formPayload = new FormData();
            
            $scope.uploadReceipt = function($event) {
                if ($('#uploadDocument').parsley().isValid())  {
                    var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Uploading Document...<br/><img class=\'uk-margin-top\' src=\'assets/img/spinners/spinner.gif\' alt=\'\'>');               
                    var file = $scope.empDataFile;                    
                    $scope.formPayload.append('fileType', $scope.info.fileType);
                    $scope.formPayload.append('nominationID', $scope.info.approved_course);	
                    $scope.formPayload.append('file', file);
                    $http({
                        url: 'dashboard/uploadFileReceipt',
                        method: 'POST',                    
                        headers: {'Content-Type': undefined},
                        data: $scope.formPayload
                    }).then(function success(response){
                        if (response.status != 200 || response.data.error) {
                            var error = (response.data.error ? response.data.error : response.statusText);
                            modal.hide();
                            $scope.message = response.data.error
                        } else {
                            modal.hide();
                            $scope.message = "Files uploaded successfully!";
                            UIkit.modal.alert('<div class=\'uk-text-center uk-text-info\'>'+$scope.message+'</div>');
                            $window.location.reload();
                            setTimeout(function(){
                                $scope.info.fileType = '';
                                $scope.empDataFile = '';
                                $scope.empDataFile = {};
                                $scope.info = {};
                                $scope.uploadDocument.$setPristine();
                            }, 200);						
                        }
                    }, function failed(xhr){
                        modal.hide();
                    });

                    $('#uploadDocument')[0].reset();
                    $('#uploadDocument').parsley().reset();

                    $scope.info.fileType = '';
                    $scope.info.excelFileUpload = {};
                    $scope.info = {};
                    $scope.uploadDocument.$setPristine();
                    
                } else {
                    $scope.message = "Sorry! Form validation error occurs. Try again with proper files."
                    UIkit.modal.alert($scope.message);				
                }
            }
            $scope.loadUploadOptions();
            
            $scope.refreshDocumentList = function() {
				
				$scope.employeeDocuments = {};
				$scope.pageData = {
					total:0,
				};
				// this function is used to get all projects
				var getProjects = function(sSource, aoData, fnCallback, oSettings){
					var draw = aoData[0].value;
					var order = aoData[2].value;
					var start = aoData[3].value;
					var length = aoData[4].value;
					var search = aoData[5].value;
					var params  = {
						start: start,
						limit: length,
						search: search,
					};
					documentService.get(params).$promise.then(function(response){
						var records = {
							'draw': 0,
							'recordsTotal': 0,
							'recordsFiltered': 0,
							'data': []
						};
						if(response.total){
							records = {
								'draw': draw,
								'recordsTotal': response.total,
								'recordsFiltered':response.total,
								'data': response.empDocuments
							};
						}

						$scope.pageData.total = response.total ? response.total : 0;
						fnCallback(records);
		            });
				};

				function rowCallback (nRow, aData, iDisplayIndex, iDisplayIndexFull) {					
					$compile(nRow)($scope);
				};

				$scope.nominatedEmployees.dtInstance = {};

				$scope.nominatedEmployees.dtOptions = DTOptionsBuilder.newOptions()
	            .withFnServerData(getProjects) // method name server call
	            .withDataProp('data')// parameter name of list use in getLeads Fuction
	            .withOption('processing', true) // required
	            .withOption('serverSide', true)// required
	            .withOption('searchable', true)// required
	            .withOption('destroy', true)// required
	            .withOption('paging', true)// required
	            .withPaginationType('full_numbers')
				.withDisplayLength(10)
				.withOption('rowCallback', rowCallback)				
	            .withOption('dom', "<'dt-uikit-header'<'uk-grid'<'uk-width-medium-1-3'l><'uk-width-medium-1-3 uk-text-center'r><'uk-width-medium-1-3'f>>>" +
	                                    "<'uk-overflow-container't>" +
	                                    "<'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'><'uk-width-medium-7-10'p>>>");
	            
	            $scope.nominatedEmployees.dtColumns = [
	               DTColumnBuilder.newColumn('DocumentName').withTitle('Document Name'),
	               DTColumnBuilder.newColumn('ContentProviderName').withTitle('Content Provider'),
				   DTColumnBuilder.newColumn('CourseName').withTitle('Course Name'),
				   DTColumnBuilder.newColumn('CourseCosting').withTitle('Course Costing'),

				   DTColumnBuilder.newColumn('CourseDuration').withTitle('Course Duration'),
				   DTColumnBuilder.newColumn('ApproveStatus').withTitle('Approve Status'),
				   DTColumnBuilder.newColumn('StatusName').withTitle('Status Name'),
                   DTColumnBuilder.newColumn('UploadedOn').withTitle('Uploaded On'),
                   
				   DTColumnBuilder.newColumn('Status').withTitle('Status'),
				   DTColumnBuilder.newColumn('Action').withTitle('Action'),
				];
			}
            $scope.refreshDocumentList();
            
            $scope.viewDocumentInfo = function(docID, emplPID) {
                $scope.documentsList = {};
				var modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Fetching Document ...<br/><div class=\'loader\'></div></div>');
				$http({
                    url: 'dashboard/getNominatedDocument',
                    method: 'POST',
                    data: {documentID: docID, empPID: emplPID}
                }).then(function successCallback(response){
                    if (!response || response.data.error) {
                        modal.hide();
                        var error = (response.data.error ? response.data.error : response.statusText);
                        UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+error+'</div>');
                    } else {
						var docData = response.data.empDocumentData;
						$scope.documentsList.course = {
							'employeeFullName': docData['FullName'],
							'employeeID': docData['EmployeeID'],
							'docName': docData['DocumentName'],
							'docID': docData['DocumentID'],
							'docUrl': docData['DocumentUrl'],
							'contProv': docData['ContentProviderName'],
							'courseName': docData['CourseName'],
                            'courseDuration': docData['CourseDuration'],                            
                            'courseStDt': docData['CourseStartDate'],
                            'courseEdDt': docData['CourseEndDate'],
						};

						UIkit.modal('#documentDialog').show();
						setTimeout(function(){
                            modal.hide();
                        }, 200);
					}
				}, function failureCallback(response){
                    modal.hide();
                    var error = (response.data.error? response.data.error: response.statusText);
                    UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+response.statusText+'</div>');
                });
                
            }
        }
    ]);