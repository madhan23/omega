angular
	.module('meritOmegaApp')
	.controller('nominationCtrl', [
		'$rootScope',
		'$scope',
		'$compile',
		'DTOptionsBuilder',
		'DTColumnBuilder',
		'$timeout',
		'$resource',
		'$http',
		'userSession',
		'$window',
		'$state',
        function ($rootScope,$scope, $compile, DTOptionsBuilder, DTColumnBuilder, $timeout, $resource, $http, userSession, $window, $state) {
			
			// $scope.initSlider = function(){
			function initSlider() {
				$(".vertical-center-4").not('.slick-initialized').slick({
					dots: true,
					vertical: true,
					centerMode: true,
					slidesToShow: 4,
					slidesToScroll: 2
				  });
				  $(".vertical-center-3").not('.slick-initialized').slick({
					dots: true,
					vertical: true,
					centerMode: true,
					slidesToShow: 3,
					slidesToScroll: 3
				  });
				  $(".vertical-center-2").not('.slick-initialized').slick({
					dots: true,
					vertical: true,
					centerMode: true,
					slidesToShow: 2,
					slidesToScroll: 2
				  });
				  $(".vertical-center").not('.slick-initialized').slick({
					dots: true,
					vertical: true,
					centerMode: true,
				  });
				  $(".vertical").not('.slick-initialized').slick({
					dots: true,
					vertical: true,
					slidesToShow: 3,
					slidesToScroll: 3
				  });
				  $(".regular").not('.slick-initialized').slick({
					dots: false,
					infinite: false,
					slidesToShow: 7,
					slidesToScroll: 7,
					  responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 6,
					slidesToScroll: 6,
					infinite: true,
					dots: true
				  }
				},
				{
				  breakpoint: 600,
				  settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			  ]
				  });
				  $(".center").not('.slick-initialized').slick({
					dots: true,
					infinite: true,
					centerMode: true,
					slidesToShow: 5,
					slidesToScroll: 3
				  });
				  $(".variable").not('.slick-initialized').slick({
					dots: true,
					infinite: true,
					variableWidth: true
				  });
				  $(".lazy").not('.slick-initialized').slick({
					lazyLoad: 'ondemand', // ondemand progressive anticipated
					infinite: true
				  });
			}
			
			var $formValidate = $('#formNominationDetail');
				$formValidate
                .parsley()
                .on('form:validated',function() {
                    $scope.$apply();
                })
                .on('field:validated',function(parsleyField) {
                    if($(parsleyField.$element).hasClass('md-input')) {
                        $scope.$apply();
                    }
				});
			
			var $formNomination = $('#formNomination');
				$formNomination
                .parsley()
                .on('form:validated',function() {
                    $scope.$apply();
                })
                .on('field:validated',function(parsleyField) {
                    if($(parsleyField.$element).hasClass('md-input')) {
                        $scope.$apply();
                    }
				});

			$scope.employeeId = userSession.get('EmployeeID');
			$scope.accessType = userSession.get('AccessType');
			$scope.maxDateOfFrom = new Date();
			$scope.minDateOfTo = new Date();
			$scope.fromDateChanged = function(fromDate){
				$scope.minDateOfTo = new Date(fromDate);
			};
			


			$scope.fromDateObject={date:new Date(new Date().getFullYear(), new Date().getMonth()-1, new Date().getDate())};
            $scope.toDateObject={date:new Date()};
			
           
           
			
			var date = new Date();
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
			$scope.monthSelectorOptions = {
				// min: firstDay,
                start: "year",
                depth: "year"
            };

			$scope.numericText = {
				format: "#",
				decimals: 0
			};

			$scope.paymentFrequency = [
				{ 'id' : 'Single', 'value' : 'Single' },
				{ 'id' : 'Weekly', 'value' : 'Weekly' },
				{ 'id' : 'Fortnightly', 'value' : 'Fortnightly' },
				{ 'id' : 'Monthly', 'value' : 'Monthly' },
				{ 'id' : 'Quarterly', 'value' : 'Quarterly' },
				{ 'id' : 'Others', 'value' : 'Others' },                    
			];

			$scope.installmentRange = function(installments, count) {
                if(count <= 12) {
                    limit = count - installments.length;
                    
                    if(count > installments.length) {
                        for(var i = 0; i < limit; i++){
                            installments.push({amount: '', actualProcessingDate: ''});
                        }
                    } else {
                        installments.splice(limit);
                    }
                
                }
			}
			
			$http({url: 'dashboard/getContentProviders', method: 'get'})
			.then(function success(response){
				$scope.contentProviders = response.data;
				$timeout(function(){
					initSlider()
				}, 150);
			});
			
			$http({
				url: 'dashboard/getNominationsCount',
				method: 'get'
			}).then(function success(response){

				var nominationsCount = response.data;
				
				$timeout(function() {
					// donut chart
					var nominationsCountsChart = '#nominationsCountChart';

					if ( $(nominationsCountsChart).length && nominationsCount) {
		
						var c3chart_donut = c3.generate({
							bindto: nominationsCountsChart,
							data: {
								columns: nominationsCount.counts,
								type : 'donut'
							},
							donut: {
								title: "Total - " + nominationsCount.total,
								width: 40,
								label: {
									format: function (value) { return value; }
								} 
							},
							color: {
								pattern: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
							}
						});
					} else {
						$(nominationsCountsChart).html("<span>No data found</span>");
					}
				},500);
			});

			$http({
				url: 'dashboard/getDivisionHeadBudget',
				method: 'get'
			}).then(function success(response){
				// donut chart

				$scope.divisionHeadBudget = response.data;
				$timeout(function() {

					

						var budgetChart = '#budgetChart';
						
						if ( $(budgetChart).length && $scope.divisionHeadBudget) {
						
							var budget = $scope.divisionHeadBudget.Consumes;
							var allocated = $scope.divisionHeadBudget.Allocated;
							
							var c3chart_donut = c3.generate({
								bindto: budgetChart,
								data: {
									columns: budget,
									type : 'donut'
								},
								donut: {
									title: Math.round(allocated),
									width: 40,
									label: {
										format: function (value) { return value; }
									} 
								},
								color: {
									pattern: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
								}
							});
	
						} else {
							$(budgetChart).html("<span>No data found</span>");
						}
						
					

				},500);
			});

			var vm = this;

			vm.getNomination = getNomination;
			vm.cancelNomination = cancelNomination;
			vm.editNomination = editNomination;
			
			vm.dtOptions = DTOptionsBuilder
				.fromFnPromise(function() {
					return $resource('dashboard/getNominations').query().$promise;
				})
				.withDOM("<'dt-uikit-header'<'uk-grid'<'uk-width-medium-2-3'l><'uk-width-medium-1-3'f>>>" +
				"<'uk-overflow-container'rt>" +
				"<'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'i><'uk-width-medium-7-10'p>>>")
				.withPaginationType('full_numbers')
				.withOption('processing', true)
				.withOption('initComplete', function() {
					$timeout(function() {
						$compile($('.dt-uikit .md-input'))($scope);
					})
				})
				
				.withOption('createdRow', function(row) {
					// Recompiling so we can bind Angular directive to the DT
					$compile(angular.element(row).contents())($scope);
				})
				
				// Active Buttons extension
				.withButtons([
					{
						extend:    'csvHtml5',
						text:      '<i class="uk-icon-file-excel-o"></i> Report',
						titleAttr: ''
					}
				]);
			
			$.extend( true, $.fn.dataTable.defaults, {
				"lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
			});
			vm.newPromise = newPromise;
			vm.reloadData = reloadData;
			vm.dtInstance = {};
			vm.dtColumns = [
				DTColumnBuilder.newColumn('requestId').withTitle('Omega ID').withOption('width', '80px')
				.renderWith(function(data,type,full) {
					return '<a class="highlight" ng-click=\'showCase.getNomination("viewNomination", '+ full.nominationId +');\'>'+ data +'</a>';
				}),
				DTColumnBuilder.newColumn('EmployeeID').withTitle('Employee ID').withOption('width', '80px'),
				DTColumnBuilder.newColumn('FullName').withTitle('Employee Name').withOption('width', '300px'),
				DTColumnBuilder.newColumn('CourseStartDate').withTitle('Course Start Date').notVisible(),
				DTColumnBuilder.newColumn('CourseEndDate').withTitle('Course End Date').notVisible(),
				DTColumnBuilder.newColumn('noOfInstallments').withTitle('No of Installments').notVisible(),
				DTColumnBuilder.newColumn('receiptUploaded').withTitle('No of receipts uploaded').notVisible(),
				DTColumnBuilder.newColumn('certificateStatus').withTitle('Certificate Status').notVisible(),
				DTColumnBuilder.newColumn('employeeLevel').withTitle('Level').notVisible(),
				DTColumnBuilder.newColumn('teamName').withTitle('TeamName').notVisible(),
				
				DTColumnBuilder.newColumn('divisionHeadName').withTitle('DivisionHead'),
				DTColumnBuilder.newColumn('reportingManagerName').withTitle('ReportingManager').notVisible(),

				DTColumnBuilder.newColumn('courseName').withTitle('Course Name'),
				DTColumnBuilder.newColumn('courseCost').withTitle('Costing').withOption('width', '60px'), 
				DTColumnBuilder.newColumn('duration').withTitle('Duration').withOption('width', '80px'),
				DTColumnBuilder.newColumn('status').withTitle('Status').withOption('width', '120px')
				.renderWith(function(data,type,full) {
					
					if(data == 'Cancelled' || data == 'Rejected')
						return '<span id="status_'+ full.nominationId +'" class="uk-badge uk-badge-danger">'+ data +'</span>';
					else if(data == 'Approved')
						return '<span id="status_'+ full.nominationId +'" class="uk-badge uk-badge-success">'+ data +'</span>';
					else
						return '<span id="status_'+ full.nominationId +'" class="uk-badge uk-badge-primary">'+ data +'</span>';
					
				})
			];
			if($scope.accessType == 'Admin') {
				vm.dtColumns.push(DTColumnBuilder.newColumn('Act Upon').withTitle('Act Upon').withOption('width', '80px').renderWith(function(data,type,full) {
					if(data == '-')
					{
						return '<p style="text-align:center !important;">-</p>';
					}else{
						return '<p style="text-align:center !important;">'+data+'</p>';
					}
				})); 
				vm.dtColumns.push(
					DTColumnBuilder.newColumn('ProgressStatus').withTitle('Progress Status').withOption('width', '80px')
					.renderWith(function(data,type,full) {
						if(full.status == 'Approved'){
							if(data == 0)
							{
								return '<div class="uk-progress uk-progress-danger uk-progress"><div class="uk-progress-bar" style="margin-left:40%;color:#000;">'+ data +'%</div></div>';
							}
							else if(data <= 30) 
							{
								return '<div class="uk-progress uk-progress-danger uk-progress"><div class="uk-progress-bar" style="width: '+data+'%	;">'+ data +' %</div></div>';

							}else if(data >= 75)
							{	
								return '<div class="uk-progress uk-progress-success uk-progress"><div class="uk-progress-bar" style="width: '+data+'%;">'+ data +' %</div></div>';
							
							}else if(data < 75 && data > 30)
							{
								return '<div class="uk-progress uk-progress-warning uk-progress"><div class="uk-progress-bar" style="width:'+data+'%;">'+ data +' %</div></div>';
							}
						}else
						{
							return '<p style="text-align:center !important;">-</p>';
						}
						
					})

				);
			}
			vm.dtColumns.push(
				DTColumnBuilder.newColumn('comments').withTitle('Comments').notVisible(),
				DTColumnBuilder.newColumn('raisedOn').withTitle('Applied on').withOption('width', '120px'),
				DTColumnBuilder.newColumn('nominationId').withTitle('File upload').withOption('width', '80px')
				.renderWith(function(data,type,full) {
					var button = "";
					if(full.status == 'Approval Pending' && ($scope.accessType == 'Self' || $scope.accessType == 'Admin' || $scope.employeeId == full.raisedEmployeeId)){
						button += '<a href="javascript:;" title="Edit nomination" ng-click=\'showCase.editNomination('+ data +');\'><i class="md-icon material-icons md-24">edit</i></a>';
					}
					if(full.status == 'Approval Pending' && ($scope.accessType == 'Self' || $scope.employeeId == full.raisedEmployeeId)){
						button += '<a href="javascript:;" title="Cancel nomination" id="cancelButton_'+ data +'" ng-click=\'showCase.cancelNomination('+ data +');\'><i class="md-icon material-icons md-24">cancel</i></a>';
					} 
					if(full.courseType == 'Paid'){
						button += '<a href="javascript:;" title="View installments" ng-click=\'showCase.getNomination("viewInstallments", '+ data +');\'><i class="md-icon material-icons md-24">view_headline</i></a>';
					}
					return '<div class="uk-text-right">'+ button +'</div>';
				})
			);
			
			

			/**
			 * view nomination details in modal
			 * @param {string}	modalName
			 * @param {integer} nominationId 
			 */

			function getNomination(modalName, nominationId){
				$formValidate.parsley().reset();
				$scope.comments = "";
				$scope.table = {};
				$http({
					url: 'dashboard/getNomination/' + nominationId,
					method: 'get',
				}).then(function success(response){
					$scope.nomination = response.data;
					UIkit.modal('#' + modalName, {modal: false,keyboard: false}).show();
				});
			}

			function editNomination(nominationId){
				
				$http({
					url: 'dashboard/getNomination/' + nominationId,
					method: 'get',
				}).then(function success(response){

					var startDate = response.data.startDate;
					var dateParts = startDate.split("-");

					$scope.maxDateOfFrom = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
					
					$scope.nomination = response.data;
					vm.isNominationEditable = true;
				});

			}
			function newPromise() {
				$rootScope.content_preloader_show('spinner');
				var fromDate = angular.element(document.getElementsByName("fromDate")).val();
				var toDate = angular.element(document.getElementsByName("toDate")).val();
				return $resource('dashboard/getNominations?fromDate='+fromDate+'&toDate='+toDate).query().$promise.then(function(result){
					$rootScope.content_preloader_hide('spinner');
					return result;
				});
			}
			function reloadData() {
				var resetPaging = true;
				vm.dtInstance.reloadData(callback, resetPaging);
			}

			function callback(json) {
				console.log(json);
			}
			vm.updateNomination = function(nomination, formNomination){

				if($formNomination.parsley().isValid() && formNomination.$valid) {
					$http({
						url: 'dashboard/updateNomination',
						method: 'patch',
						data: nomination
					}).then(function success(response){
						if(response.status) {
							alert('Updated successfully');
						}
					});
				}

			}

			$scope.checkInstallmentAmount = function(courseCost, installments, formNomination) {
				var installmentTotal = 0;
				for(var i = 0; i < installments.length; i++) {
					installmentTotal = parseInt(installmentTotal) + parseInt(installments[i].amount);
				}
				formNomination.courseCost.$setValidity('unique', Boolean(installmentTotal == courseCost));
				vm.installmentHasError = !Boolean(installmentTotal == courseCost);
			}

			function cancelNomination(nominationId) {
				UIkit.modal.prompt('Are you sure want to cancel this request?', '', function(comments){ 
					if(comments) {
						$http({
							url: 'dashboard/updateNominationStatus',
							method: 'patch',
							data: {nominationId: nominationId, comments: comments, status: 'Cancelled'}
						}).then(function success(response){
							var status = response.data.status;
							
							$("#status_"+ nominationId).text(status).addClass('uk-badge-danger');
							$("#cancelButton_"+ nominationId).hide();
						});
					}
					else
						return false;
				},{labels: {'Ok': 'Yes', 'Cancel': 'No'}});
			}

			$scope.updateInstallmentStatus = function(installment, statusId) {
				
				$http({
					url: 'dashboard/updateInstallmentStatus',
					method: 'patch',
					data: {id: installment.id, statusId: statusId} 
				}).then(function success(response){
					installment.status = response.data.status;
					installment.statusId = response.data.statusId;
				});

			}

			$scope.updateInstallmentReceipt = function(element) {
				
				var file = element.files[0];
				var fileName = file.name;
				var fileType = fileName.split('.').pop();

				if(fileType == 'pdf'){
					var formData = new FormData();
					formData.append('installmentId', element.id);
					formData.append('file', file);
					$http({
						url: 'dashboard/updateInstallmentReceipt',
						method: 'post',
						headers: { 'Content-Type': undefined },
						data: formData
					}).then(function success(response){
						if(response.status) {
							alert('File Uploaded successfully');
						}
						// $('#installmentStatus_' + element.id).text(response.data.status);
					});
				} else {
					alert('Please upload only pdf files');
				}

			}
			
			$scope.updateInstallmentReceiptStatus = function(installment, statusId) {
				
				$http({
					url: 'dashboard/updateInstallmentReceiptStatus',
					method: 'patch',
					data: {id: installment.id, statusId: statusId} 
				}).then(function success(response){
					installment.receiptStatus = response.data.receiptStatus;
				});

			}
			
			$scope.selectAll = function(value, installments) {
				for (var i = 0; i < installments.length; i++) {
					if(installments[i].status != 'PayProcess-Completed') {
						installments[i].selected = value;
					}
				}
			}

			/**
			 * Currently not needed
			 */
			/*
			$scope.updateInstallment = function(installment) {
				$http({
					url: 'dashboard/updateInstallment',
					method: 'patch',
					data: installment
				}).then(function success(response){
					
				});
			}
			*/

			/**
			 * updating selected installments
			 * @param {object} installments 
			 * @param {integer} statusId 
			 */
			$scope.updateInstallments = function(installments, statusId) {
				
				for(var i = 0; i < installments.length; i++) {
					if(installments[i].selected) {
						installments[i].statusId = statusId;
					}
				}
				
				$http({
					url: 'dashboard/updateInstallments',
					method: 'patch',
					data: installments
				}).then(function success(response){
					for(var i = 0; i < installments.length; i++) {
						installments[i].status = response.data[i].status;
					}
				});

			}

			/**
			 * updating nomination status
			 * @param {integer} nominationId 
			 * @param {string} comments 
			 * @param {string} status 
			 */
			$scope.updateNominationStatus = function(nominationId, comments, status){
				if ($formValidate.parsley().isValid()) {
					var currDate  = new Date(moment(new Date()));
					var startDate = new Date(moment($scope.nomination.startDate,"DD-MM-YYYY"));
					if($scope.nomination.paidBy == 'Company' && status == 'Approved' && startDate < currDate ){
						UIkit.modal.confirm('Course start date is earlier than approval date. Would you like to change the course start date and approve accordingly?', function(){
							UIkit.modal('#viewNomination').hide();
							editNomination(nominationId);
						},{labels: {'Ok': 'Yes', 'Cancel': 'No'}});
					}
					else{
						$http({
							url: 'dashboard/updateNominationStatus',
							method: 'patch',
							data: {nominationId: nominationId, comments: comments, status: status}
						}).then(function success(response){
							var status = response.data.status;
							var spanClass = 'uk-badge-primary';
							
							if(status == 'Approved') {
								spanClass = 'uk-badge-success';
							} else if(status == 'Cancelled' || status == 'Rejected') {
								spanClass = 'uk-badge-danger';
							} 
							$("#status_"+ nominationId).text(status).removeClass('uk-badge-success uk-badge-danger').addClass(spanClass);
							$scope.comments = "";
							UIkit.modal('#viewNomination').hide();
						});
					}
					
				}
			}
			$scope.completeNomination = {};
			$scope.uploadCertificate = function(id){
				var element = document.getElementById(''+id+'');
				var file = element.files[0];
				if(file == undefined){
					UIkit.modal.alert('Please upload the certificate to proceed further..!!');
				}
				else if(file.name.split('.').pop() == 'pdf')
				{
					$('#completeNominationForm').parsley().validate();
					if ($('#completeNominationForm').parsley().isValid() && file != undefined) {
						var formData = new FormData();
						formData.append('nominationId', id);
						formData.append('file', file);
						formData.append('comments', $scope.completeNomination.Comments);
						$http({
							url: 'dashboard/uploadCertificate',
							method: 'post',
							headers: { 'Content-Type': undefined },
							data: formData
						}).then(function success(response){
							if(response.data.AffectedRows > 0) {
								UIkit.modal.alert('File Uploaded successfully');
							}else{
								UIkit.modal.alert('Error uploading file, Please try again..!!');
							}
						});
					}
				}else
				{
					UIkit.modal.alert('Please upload the file in Pdf format..!!');
				}
				
				
			}
			$scope.refresh = function(){
				$state.reload();
			};
			
        }
    ]);
