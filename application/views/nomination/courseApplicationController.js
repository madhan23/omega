angular
    .module('meritOmegaApp')
    .controller('courseApplicationCtrl', [
        '$scope',
        '$rootScope',
        '$http',
		'$filter',
		'$compile',
		'DTOptionsBuilder',
		'DTColumnBuilder',
		'employeeDeps',
        'utils',
		'$location',
		'$state',
        function ($scope,$rootScope,$http,$filter,$compile,DTOptionsBuilder,DTColumnBuilder,employeeDeps,utils,$location, $state) {
            
            $http({url: 'dashboard/getEligibleForNomination', method: 'get'})
			.then(function success(response){
                $scope.eligibleForNomination = response.data;
            }); 
            
            
            var $formValidate = $('#formApplyCourse');
            $formValidate
            .parsley()
            .on('form:validated',function() {
                $scope.$apply();
            })
            .on('field:validated',function(parsleyField) {
                if($(parsleyField.$element).hasClass('md-input')) {
                    $scope.$apply();
                }
            });
            
            
           
            // masked inputs
			var $maskedInput = $('.masked_input');
			if($maskedInput.length) {
				$maskedInput.inputmask();
			}

			$scope.numericText = {
				format: "#",
				decimals: 0
			}

			$scope.paymentFrequency = [
				{ 'id' : 'Single', 'value' : 'Single' },
				{ 'id' : 'Weekly', 'value' : 'Weekly' },
				{ 'id' : 'Fortnightly', 'value' : 'Fortnightly' },
				{ 'id' : 'Monthly', 'value' : 'Monthly' },
				{ 'id' : 'Quarterly', 'value' : 'Quarterly' },
				{ 'id' : 'Others', 'value' : 'Others' },                    
			];

            $scope.installments = [];
			$scope.installmentRange = function(count) {
                if(count <= 12) {
                    limit = count - $scope.installments.length;
                    
                    if(count > $scope.installments.length) {
                        for(var i = 0; i < limit; i++){
                            $scope.installments.push({amount: '', processDate: ''});
                        }
                    } else {
                        $scope.installments.splice(limit);
                    }
                
                }
            }

			$http({url: 'dashboard/getContentProviders', method: 'get'})
			.then(function success(response){
				$scope.contentProviders = response.data;
			});

			$scope.installmentClass = false;

			$scope.applyCourse = function() {
                if (
					$('#formApplyCourse').parsley().isValid() 
					&& 
					(
						$scope.course.type == 'Free' || ($scope.course.type == 'Paid' && $scope.installmentClass)
					) 
				){
                    $rootScope.content_preloader_show('spinner_success');
                    $scope.enableToApply = true;
                    $scope.course.installments = $scope.installments;
                    $scope.course.AutoApprove = 0;
					$http({
                        url: 'dashboard/applyCourse',
                        method: 'POST',
                        data: {
	                        formData: $scope.course
	                    }
                    }).then(function success(response) {
                        UIkit.modal.alert('<div class=\'uk-text-center uk-text-success\'>Your OMEGA nomination '+ response.data.requestId +' has been submitted to the next stage. <br />You will receive further information once the Divisional Head confirms on the same. <br /><br />We wish you the very best in this exciting learning journey</div>');
                        $rootScope.content_preloader_hide();
                        $scope.enableToApply = false;
                        $state.go('restricted.nomination');
                    }, function error(response) {
                        $rootScope.content_preloader_hide();
                        $scope.enableToApply = false;
                        
                });
				}else{
                    $rootScope.content_preloader_hide();
                    $scope.enableToApply = false;
                }
			}

			$scope.maxDateOfFrom = new Date();
			$scope.minDateOfTo = new Date();
			
            $scope.fromDateChanged = function(fromDate){
				$scope.minDateOfTo = new Date(fromDate);
			};
			


			
			$scope.disableOption = false;
            $scope.enableInstallments = false;
            $scope.enableOptionDetail = false;
            $scope.enableSpecPayment = false;
            $scope.disableInstallments = true;
            $scope.incrementInstallment = true;
            
            $scope.installmentMonthClass = true;

			var $wizard_advanced_form = $('#wizard_advanced_form');
			
            $scope.exitValidation = function() {
                return true;
            }
			
			/*
            $scope.finishedWizard = function() {
				var form_serialized = utils.serializeObject($wizard_advanced_form);
				form_serialized.installments = $scope.installments;
				//form_serialized = JSON.stringify(utils.serializeObject($wizard_advanced_form), null, 2 );
				$('#wizard_advanced_form').parsley().validate();
                if ($('#wizard_advanced_form').parsley().isValid()) {
                    $http({
                        url: 'dashboard/addCourseRequest',
                        method: 'POST',
                        data: {
                            formData: form_serialized
                        }
                    }).then(function successCallback(response) {
                        if (!response || response.data.error) {
                            var error = (response.data.error? response.data.error: response.statusText);
                            UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+error+'</div>');
                        }
                        else {
                            $('#wizard_advanced_form').parsley().reset();
                            UIkit.modal.alert('<div class=\'uk-text-center uk-text-success\'>Your OMEGA nomination has been submitted to the next stage. <br />You will receive further information once the Divisional Head confirms on the same. <br /><br />We wish you the very best in this exciting learning journey</div>');
                            $location.path( "/nomination" );
                        }
                    }, function failureCallback(response) {
                        var error = (response.data.error? response.data.error: response.statusText);                        
                        UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+response.statusText+'</div>');                        
                    });
                }
                else {
                    UIkit.modal.alert('Please provide all required details.');
                }
			};
			*/

			$scope.monthSelectorOptions = {
				min: new Date(),
                start: "year",
                depth: "year"
            };
			
			

            if (!employeeDeps || employeeDeps.data.error) {
            	var error = employeeDeps.data.error? employeeDeps.data.error: employeeDeps.statusText;
            	UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Sorry !<br>'+error+'</div>');
            }
            else {
				var contentProvidersList = employeeDeps.data.content_provider;
				/*
				$scope.nomination = {
                    'employee' : {
                        'fullname' : '',
                        'employeeID': '',
                        'teamname' : '',
                        'division' : '',
                        'rmfname' : '',
                        'rmEmpID' : '',
                        'dhfname' : '',
                        'dhEmpID' : '',
                        'EmpPID':'',
                        'RM1PID':'',
                        'DHPID':'',
                        'TeamID':'',
                        'DivisionID':'',
                    },
                    'course'    : {
                        'content_provider' : '',
                        'name' : '',
                        'cost' : 0,
                        'duration' : '',
                        'start_date' : '',
                        'end_date' : '',
                        'type': '',
                    },
                    'confirmation' : {
                        'empComments' : '',
                        'term1' : '',
                        'term2' : '',
                    }
				};
				var employeeDetails = [{
					'PID': employeeDeps.data.empDetails[0]['PID'],
					'Fullname': employeeDeps.data.empDetails[0]['FullName']
				}];
				var teamName = [{
					'name': employeeDeps.data.empDetails[0]['TeamName']
				}];

				var reportingManager = [
					{
						'Fullname': employeeDeps.data.empDetails[0]['RM1Fname'],
						'PID': employeeDeps.data.empDetails[0]['RM1PID']
					},
					{
						'Fullname': employeeDeps.data.empDetails[0]['RM2Fname'],
						'PID': employeeDeps.data.empDetails[0]['RM2PID']
					},
                ];
                
                var payment_types = [
                    { 'id' : 'Single', 'value' : 'Single' },
                    { 'id' : 'Weekly', 'value' : 'Weekly' },
                    { 'id' : 'Fortnightly', 'value' : 'Fortnightly' },
                    { 'id' : 'Monthly', 'value' : 'Monthly' },
                    { 'id' : 'Quarterly', 'value' : 'Quarterly' },
                    { 'id' : 'Others', 'value' : 'Others' },                    
                ];
				
				$scope.k_employee_list = {
					dataSource: employeeDetails,
                    dataTextField: 'Fullname',
                    dataValueField: 'PID'
                };
                $scope.k_payment_type_list = {
					dataSource: payment_types,
                    dataTextField: 'id',
                    dataValueField: 'value'
				};
                $scope.k_teams_list = {
					dataSource: teamName,
					dataTextField: 'name',
					dataValueField: 'name'
				};
                $scope.k_reporting_manager_list = {
					dataSource: reportingManager,
					dataTextField: 'Fullname',
                    dataValueField: 'PID'
                };
                $scope.k_content_provider_list = {
                    dataSource : contentProvidersList,
                    dataTextField: 'ContentProviderName',
                    dataValueField: 'ContentProviderName'
                };
                $scope.nomination.employee = {
                    'PID': employeeDeps.data.empDetails[0]['PID'],
                    'fullname': employeeDeps.data.empDetails[0]['FullName'],
                    'employeeID': employeeDeps.data.empDetails[0]['EmployeeID'],
                    'teamname': employeeDeps.data.empDetails[0]['TeamName'],
                    'division': employeeDeps.data.empDetails[0]['Division'],
                    'rmfname': employeeDeps.data.empDetails[0]['RM1Fname'],
                    'rmEmpID': employeeDeps.data.empDetails[0]['RM1EmpID'],
                    'dhfname': employeeDeps.data.empDetails[0]['DivHeadFName'],
                    'dhEmpID': employeeDeps.data.empDetails[0]['DivHEmpID'],
                    'TeamID': employeeDeps.data.empDetails[0]['TeamID'],
                    'DivisionID': employeeDeps.data.empDetails[0]['DivisionID'],
                    'EmpPID':employeeDeps.data.empDetails[0]['PID'],
                    'RM1PID':employeeDeps.data.empDetails[0]['RM1PID'],
                    'DHPID':employeeDeps.data.empDetails[0]['DivHPID'],
                };
				*/
                $scope.enableSpecificPayment = function() {
                    if($scope.course.paymentFrequency == 'Others') {
                        $scope.enableSpecPayment = true;
                        $scope.disableInstallments = true;
                        $scope.incrementInstallment = true;
                    }
                    else if($scope.course.paymentFrequency == 'Single') {
                        $scope.enableSpecPayment = false;
                        $scope.disableInstallments = true;
                        $scope.installments = [];
                        $scope.course.noOfInstallments = 1;
                        $scope.incrementInstallment = false;
                        $scope.installmentRange(1);
                    }
                    else {
                        $scope.enableSpecPayment = false;
                        $scope.disableInstallments = true;
                        $scope.incrementInstallment = true;
                    }
                    
                }
				
				
                $scope.checkInstallmentAmount = function() {
                    var installmentTotal = 0;
                    if(angular.isUndefined($scope.course.cost)) {                        
                        UIkit.modal.alert('<div class=\'uk-text-center uk-text-danger\'>Please fill in the Total Cost !</div>');
                    }
                    else {                        
                        var installmentCount = $scope.course.noOfInstallments;                        
                        for(var i=0; i <= installmentCount-1; i++) {
                            installmentTotal = parseInt(installmentTotal) + parseInt($scope.installments[i].amount);
                        }
                        if(installmentTotal == $scope.course.cost) {
                            $scope.installmentClass = true;
                        }
                        else {
                            $scope.installmentClass = false;
                        }
                    }                    
                }
				/*
                var checkduplicates = function(a) {
                    if(a.length > 1) {
                        
                        for(var i = 0; i <= a.length; i++) {
                            for(var j = i; j <= a.length; j++) {
                                if(i != j && a[i] == a[j]) {
                                    return true;
                                }
                            }
                        }
                        return false;
                    }
                    else {
                        return false;
                    }
                    
                }
				*/
				/*
                $scope.checkInstallmentMonth = function() {
                    var installmentCount = $scope.course.noOfInstallments;
                    var installmentMonth = [];                     
                    for(var i=0; i <= installmentCount-1; i++) {
                        if($scope.installments[i].processDate != '') {
                            installmentMonth.push($scope.installments[i].processDate);
                        }
                    }
					
					if(checkduplicates(installmentMonth)) {
                        $scope.installmentMonthClass = false;
                    }
                    else {
                        $scope.installmentMonthClass = true;
					}
				}
				*/

                $scope.check_installments = function() {
                    if($scope.nomination.course.payment_type != 'Single') {
                        $scope.enableInstallments = true;
                        if($scope.nomination.course.payment_type == 'Others') {
                            $scope.enableOptionDetail = true;
                        }
                        else {
                            $scope.enableOptionDetail = false;
                        }
                    }
                    else {
                        $scope.enableInstallments = false;
                        $scope.enableOptionDetail = false;
                    }
				}
				
				
			}

        }
    ]);
