<div id="page_content">
    <div id="page_content_inner" ng-controller="nominationCtrl as showCase">

		<div class="uk-accordion uk-margin-bottom" ng-show="accessType == 'DivisionHead' || accessType == 'Admin'" data-uk-accordion>
			<h3 class="uk-accordion-title">Charts</h3>
			<div class="uk-accordion-content">
				<div class="uk-grid" data-uk-grid-margin>
					<div class="uk-width-medium-1-3">
						<div class="md-card">
						<!--chartist data="chartist_stat_data" options="chartist_stat_options" responsive-options="chartist_stat_options_responsive" ng-id="chartist_stats" type="bar"></chartist-->
							<div class="md-card-content">
								<h4 class="heading_c uk-margin-bottom">Nomination statistics</h4>
								<div id="nominationsCountChart" class="c3chart"></div>
							</div>
						</div>
					</div>
					<div ng-show="divisionHeadBudget" class="uk-width-medium-1-3">
						<div class="md-card">
							<div class="md-card-content">
								<h4 class="heading_c uk-margin-bottom">Budget - {{divisionHeadBudget.FullName}}</h4>
								<div id="budgetChart" class="c3chart"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div  ng-show="!showCase.isNominationEditable" class="md-card uk-container-center">
			<div class="md-card-content large-padding">
				<div class="cprovider">
					<!-- <h4 class="heading_c uk-margin-bottom">Online Content Providers</h4> -->
					<section class="regular slider">
						<div ng-repeat="contentProvider in contentProviders">
							<a href="{{contentProvider.URL}}" target="_blank">
								<img ng-src="{{contentProvider.Path}}" />
							</a>
						</div>
					</section>
				</div>
				<h4 class="heading_a uk-margin-bottom">Nominations</h4>
				<form id="SearchForm">
					<div class="uk-grid" data-uk-grid-margin>
					</div>
					<div class="uk-grid uk-margin-small-top uk-margin-bottom" data-uk-grid-margin>
						<div class="uk-width-medium-1-5 parsley-row">
							<span class="uk-form-help-block" style="margin:-12px 0 0;">From Date</span>
							<input 
								id="fromDate"
								name="fromDate"
								style="box-shadow:none !important;width:100%;"
								kendo-date-picker
								placeholder="From"
								k-ng-model="fromDateObject.date"
								k-rebind="maxDateOfFrom"
								k-format="'dd-MM-yyyy'"
								k-on-change="fromDateChanged(fromDateObject.date)"
								ng-required="true"
							/>
						</div>
						<div class="uk-width-medium-1-5 parsley-row">
							<span class="uk-form-help-block" style="margin:-12px 0 0;">To Date</span>
							<input 
								id="toDate" 
								name="toDate"
								style="box-shadow:none !important;width:100%;"
								kendo-date-picker
								placeholder="To"
								k-ng-model="toDateObject.date"
								k-min = "minDateOfTo"
								k-rebind = "minDateOfTo"
								k-format="'dd-MM-yyyy'"
								ng-required="true"
							/>
						</div>
					</div>
					<div class="uk-grid uk-margin-small-top uk-margin-medium-bottom" data-uk-grid-margin>
						<div class="uk-width-medium-1-1">
							<a ng-click="showCase.dtInstance.changeData(showCase.newPromise);" class="md-btn md-btn-primary">Search</a>
							<a ng-click="refresh();" class="md-btn md-btn-danger">Reset</a>
						</div>
					</div>
				</form>
				<table
					datatable
					dt-options="showCase.dtOptions"
					dt-columns="showCase.dtColumns"
					dt-instance="showCase.dtInstance"
					class="uk-table"
					cellspacing="0"
					width="100%"
				></table>
			</div>
		</div>

		<div class="uk-modal" id="viewNomination">
			<div class="uk-modal-dialog uk-modal-dialog-large">
				<button type="button" class="uk-modal-close uk-close"></button>
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">
						Nomination ID - {{nomination.requestId}}
						<span
							id="status_{{nomination.nominationId}}"
							class="uk-badge uk-badge-primary"
							ng-class="{
								'uk-badge-success': nomination.status == 'Approved',
								'uk-badge-danger': (nomination.status == 'Cancelled' || nomination.status == 'Rejected')
							}"
						>
							{{nomination.status}}
						</span>
					</h3>
				</div>
				<div class="md-card-content large-padding">
					<div class="uk-grid uk-grid-divider uk-grid-medium">
						<div class="uk-width-large-1-2">
							<table class="uk-table">
								<tbody>
									<tr>
										<td>No of Nominations Completed</td>
										<td>
											{{nomination.noOfNominations}}
										</td>
									</tr>	
									<tr>
										<td>Team Name</td>
										<td>
											{{nomination.TeamName}}
										</td>
									</tr>
									<tr>
										<td width="35%">Reporting Manager</td>
										<td>
											{{nomination.ReportingManager}}
										</td>
									</tr>
									<tr>
										<td>Course Name</td>
										<td>
											{{nomination.courseName}}
											<br/>
											{{nomination.courseUrl}}
										</td>
									</tr>
									<tr>
										<td>Course Type</td>
										<td>
											{{nomination.courseType}}
										</td>
									</tr>
									<tr>
										<td>Content Provider</td>
										<td>
											{{nomination.contentProvider}}
										</td>
									</tr>
									<tr>
										<td>Applied on</td>
										<td>
											{{nomination.raisedOn}}
										</td>
									</tr>
									<tr>
										<td>Duration</td>
										<td>
											{{nomination.duration}}
											from - {{nomination.startDate}} to - {{nomination.endDate}}
										</td>
									</tr>
									<tr ng-show="nomination.courseType == 'Paid'">
										<td>Payment Type</td>
										<td>
											{{nomination.paidBy}}
										</td>
									</tr>
									<tr ng-show="nomination.courseType == 'Paid'">
										<td>Total cost (in INR)</td>
										<td>
											{{nomination.courseCost}}
										</td>
									</tr>
									<tr ng-show="nomination.courseType == 'Paid'">
										<td>Payment frequency</td>
										<td>
											{{nomination.paymentFrequency}}
										</td>
									</tr>
									<tr ng-show="nomination.courseType == 'Paid'">
										<td>Installments</td>
										<td>
											{{nomination.noOfInstallments}}
											<i 
												class="md-icon material-icons"
												data-uk-tooltip="{cls:'long-text'}"
												title="{{nomination.installmentsForTooltip}}"
											>&#xE88F;</i>
										</td>
									</tr>
									<tr>
										<td>Nomination Status</td>
										<td>
											<span>{{nomination.isOpen == 1 ? 'Open' : 'Closed'}}</span>
										</td>
									</tr>
									<tr ng-show="nomination.certificateStatus == 'Certificate Uploaded'">
										<td>Certificate Comments</td>
										<td><p style="word-wrap: break-word;word-break: break-all;">{{nomination.certificateComments}}</p></td>
									</tr>
									<tr ng-show="nomination.certificateStatus == 'Certificate Uploaded'">
										<td>Certificate UploadedOn</td>
										<td>
											{{nomination.certificateUploadedOn}} 
											<span>
												<i class="md-icon material-icons" data-uk-tooltip="{cls:'long-text'}"
												title="Uploaded By <br> {{nomination.certificateUploadedBy}}">person</i>
											</span>
											<a ng-href="dashboard/viewFile/{{nomination.certificateName}}/certificate" target="_blank" title="View Certificate">
												<i class="md-icon material-icons">launch</i>
											</a>
										</td>
									</tr>
								</tbody>
							</table>
							<form id="completeNominationForm">
								<div ng-if="nomination.status == 'Approved' && nomination.EmployeeID == employeeId && nomination.certificateStatus != 'Certificate Uploaded'">
									<div class="uk-grid uk-margin-small-top" data-uk-grid-margin>
										<div style="width:100%;" class="uk-width-1-1 parsley-row">
											<div class="uk-float-left" style="width:90%;">
												<textarea
													class="md-input"
													cols="10"
													rows="1"
													placeholder="Comments"
													required
													md-input
													data-parsley-trigger="keyup"
													data-parsley-minlength="50" 
													data-parsley-maxlength="500"
													ng-model="completeNomination.Comments"
												></textarea>
											</div>
											<div class="uk-float-right">
												<span class="btn-file">
												<span class="fileinput-new">
													<i class="md-icon material-icons md-24">publish</i>
												</span>
												<input 
												title="Upload Certificate" 
												id="{{nomination.nominationId}}" 
												accept="application/pdf" 
												type="file" 
												/>
												</span>
											</div>
											<div style="clear : both;"></div>
										</div>
										<div>
											<a ng-click="uploadCertificate(nomination.nominationId)" class="md-btn md-btn-success uk-margin-top">Complete Nomination</a>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="uk-width-large-1-2">
							<p style="word-wrap: break-word;">
								<span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">Reason</span>
								{{nomination.comments}}
							</p>
							<hr class="uk-grid-divider">
							<div style="min-height:200px;max-height:500px;overflow:auto;">
								<ul class="md-list">
									<li ng-repeat="timeline in nomination.timeline">
										<div class="md-list-content">
											<span class="md-list-heading">{{timeline.comments}}</span>
											<div class="uk-margin-small-top">
												<span class="uk-margin-right">
													<i class="material-icons">person</i> <span class=" uk-text-small">{{timeline.updatedBy}}</span>
												</span>
												<span class="uk-margin-right">
													<i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">{{timeline.updatedOn}}</span>
												</span>
												<span class="uk-badge uk-badge-primary">{{timeline.status}}</span>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<form
						ng-show="
							((accessType == 'DivisionHead' && employeeId != nomination.EmployeeID) || accessType == 'Admin')
							&&
							(nomination.status == 'Approved' || (nomination.status != 'Cancelled' && nomination.status != 'Rejected'))
							&& 
							nomination.isOpen == 1
						"
						id="formNominationDetail"
						class="uk-form-stacked"
					>
						<!--div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-medium-1-4 parsley-row">
								<label class="uk-form-label">Training Type<span class="req">*</span></label>
								<span class="icheck-inline">
									<input type="radio" id="radio_online" name="trainingType" ng-model="action.trainingType" icheck required value="Online" />
									<label for="radio_online" class="inline-label">Free</label>
								</span>
								<span class="icheck-inline">
									<input type="radio" id="radio_offline" name="trainingType" ng-model="nomination.trainingType" icheck value="Offline" />
									<label for="radio_offline" class="inline-label">Paid</label>
								</span>
							</div>
						</div-->
						<div class="uk-grid uk-margin-small-top" data-uk-grid-margin>
							<div class="uk-width-1-1 parsley-row">
								<textarea
									class="md-input"
									cols="10"
									rows="1"
									placeholder="Comments"
									required
									md-input
									data-parsley-trigger="keyup"
									data-parsley-minlength="100" data-parsley-maxlength="200"
									data-parsley-validation-threshold="10"
									ng-model="comments"
								></textarea>
							</div>
						</div>
						<div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-1-1">
								<button
									ng-show="nomination.status != 'Approved'"
									class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light"
									type="submit"
									ng-click="updateNominationStatus(nomination.nominationId, comments, 'Approved');"
								>Approve</button>
								<button
									class="md-btn md-btn-danger md-btn-wave-light waves-effect waves-button waves-light"
									type="submit"
									ng-click="updateNominationStatus(nomination.nominationId, comments, 'Rejected');"
									class="md-btn md-btn-primary"
								>Reject</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>

		<div class="uk-modal" id="viewInstallments">
			<div class="uk-modal-dialog">
				<button type="button" class="uk-modal-close uk-close"></button>
				<table class="uk-table">
					<thead>
						<tr>
							<!--th
								ng-show="
									(accessType == 'ReportingManger' || accessType == 'DivisionHead' || accessType == 'Admin')
									&&
									!(installments | filter:{status: 'Approval Pending'}).length
								"
								class="uk-width-1-10 uk-text-center small_col"
							>
								<input type="checkbox" icheck ng-change="selectAll(table.selectAll, installments);" ng-model="table.selectAll" />
							</th-->
							<th class="uk-width-3-10">Installment date</th>
							<th class="uk-width-2-10">Amount</th>
							<th class="uk-width-1-10 uk-text-center">Status</th>
							<th ng-show="nomination.status != 'Approval Pending'" class="uk-width-4-10 uk-text-center">Actions</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="installment in nomination.installments">
							<!--td
								ng-show="
									(accessType == 'ReportingManger' || accessType == 'DivisionHead' || accessType == 'Admin')
									&&
									!(installments | filter:{status: 'Approval Pending'}).length
								"
								class="uk-text-center uk-table-middle small_col"
							>
								<input
									type="checkbox"
									icheck
									ng-model="installment.selected"
								/>
							</td-->
							<td>{{installment.actualProcessingDate}}</td>
							<td>
								<span ng-show="!isEditable">{{installment.amount}}</span>
								<input
									ng-show="isEditable"
									kendo-numeric-text-box
									k-min="0"
									k-ng-model="installment.amount"
									style="width: 60%;"
									required
								/>
							</td>
							<td>
								<span
									id="installmentStatus_{{installment.id}}"
									class="uk-badge"
									ng-class="{
										'uk-badge-default': installment.status == 'Requested',
										'uk-badge-success': installment.status == 'PayProcess-Completed',
										'uk-badge-warning': installment.status == 'Hold',
										'uk-badge-danger': installment.status == 'Cancelled'
									}"
								>
									{{installment.status}}
								</span>
								<span
									ng-if="installment.receipt"
									class="uk-badge"
									ng-class="{
										'uk-badge-primary': installment.receiptStatus == 'Receipt Uploaded',
										'uk-badge-success': installment.receiptStatus == 'Receipt Verfied',
										'uk-badge-danger': installment.receiptStatus == 'Receipt Invalid'
									}"
								>
									{{installment.receiptStatus}}
								</span>
							</td>
							<td ng-show="nomination.status != 'Approval Pending'" class="uk-text-center">
								<!--
								<a href="javascript:;" ng-show="!isEditable" ng-click="isEditable = true;">
									<i class="md-icon material-icons">&#xE254;</i>
								</a>
								<a href="javascript:;" ng-show="isEditable" ng-click="isEditable = false; updateInstallment(installment);">
									<i class="md-icon material-icons">save</i>
								</a>
								-->
								
								<a 	ng-show="
										installment.status != 'PayProcess-Completed'
										&&
										installment.status == 'Requested'
										&&
										(accessType == 'ReportingManger' || accessType == 'DivisionHead' || accessType == 'Admin')
									"
									href="javascript:;"
									ng-click="updateInstallmentStatus(installment, 8);"
									title="Hold installment"
								>
									<i class="md-icon material-icons">pause_circle_outline</i>
								</a>
								<a 	ng-show="
										installment.status != 'PayProcess-Completed'
										&&
										installment.status == 'Hold'
										&&
										(accessType == 'ReportingManger' || accessType == 'DivisionHead' || accessType == 'Admin')
									"
									href="javascript:;"
									ng-click="updateInstallmentStatus(installment, 7);"
									title="Resume installment"
								>
									<i class="md-icon material-icons">play_circle_outline</i>
								</a>

								<a ng-show="installment.receipt" ng-click="installment.canReceiptVerify = true" ng-href="dashboard/viewFile/{{installment.receipt}}/receipt" target="_blank" title="View receipt">
									<i class="md-icon material-icons">launch</i>
								</a>
								
								<a 
									ng-show="
										installment.canReceiptVerify
										&&
										(installment.receiptStatus == 'Receipt Uploaded' || installment.receiptStatus == 'Receipt Invalid')
										&&
										(accessType == 'ReportingManger' || accessType == 'DivisionHead' || accessType == 'Admin')
									"
									href="javascript:;"
									ng-click="updateInstallmentReceiptStatus(installment, 6);"
									title="Receipt verified"
								>
									<i class="md-icon material-icons">done</i>
								</a>
								<a 
									ng-show="
										installment.receipt
										&&
										(installment.receiptStatus == 'Receipt Uploaded' || installment.receiptStatus == 'Receipt Verified')
										&&
										(accessType == 'ReportingManger' || accessType == 'DivisionHead' || accessType == 'Admin')
									"
									href="javascript:;"
									ng-click="updateInstallmentReceiptStatus(installment, 9);"
									title="Receipt invalid"
								>
									<i class="md-icon material-icons">cancel</i>
								</a>
								<span ng-show="(installment.status != 'Approval Pending' && installment.status != 'Cancelled' && installment.status != 'Rejected') && employeeId == nomination.EmployeeID && installment.receiptStatus != 'Receipt Verfied'" class="btn-file">
									<span class="fileinput-new"><i class="md-icon material-icons md-24">publish</i></span>
									<input id="{{installment.id}}" accept="application/pdf" type="file" onchange="angular.element(this).scope().updateInstallmentReceipt(this);">
								</span>
								
							</td>
						</tr>
					</tbody>
				</table>
				<!--div
					ng-show="
						(accessType == 'ReportingManger' || accessType == 'DivisionHead' || accessType == 'Admin')
						&&
						!(installments | filter:{status: 'Approval Pending'}).length
					"
				>
					<button 
						type="button"
						ng-class="{'disabled': !(installments | filter:{selected: true}).length}"
						ng-click="updateInstallments(installments, 3)"
						class="md-btn md-btn-primary"
						ng-show="nomination.statusId != 4"
					>Resume</button>
					<button
						type="button"
						ng-class="{'disabled': !(installments | filter:{selected: true}).length}"
						ng-click="updateInstallments(installments, 8)"
						class="md-btn md-btn-warning"
						ng-show="nomination.statusId != 4"
					>Hold</button>
					<button
						type="button"
						ng-class="{'disabled': !(installments | filter:{selected: true}).length}"
						ng-click="updateInstallments(installments, 4)"
						class="md-btn md-btn-danger"
						ng-show="nomination.statusId != 4"
					>Cancel</button>
				</div-->
			</div>
		</div>

		<div class="md-card">
			<div ng-show="showCase.isNominationEditable" class="md-card-content">
				<h3 class="heading_b uk-margin-small-bottom">
					Nomination - {{nomination.requestId}}
					<a
						class="md-fab md-fab-small uk-float-right"
						href="javascript:void(0)"
						data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}"
						title="Close"
						ng-click="showCase.isNominationEditable = false"
					>
						<i class="material-icons">close</i>
					</a>
				</h3>
				
				<h3 class="heading_a uk-margin-small-bottom">Course information</h3>
				<form id="formNomination" name="formNomination" class="uk-form-stacked">
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-2">
							<div class="parsley-row">
								<label for="name">Topic / Course name<span class="req">*</span></label>
								<input id="name" type="text" required class="md-input" md-input ng-model="nomination.courseName"/>
							</div>
						</div>
						<div class="uk-width-medium-1-2">
							<div class="parsley-row">
								<label for="url">Course url<span class="req">*</span></label>
								<input id="url" type="text" required class="md-input" md-input ng-model="nomination.courseUrl"/>
							</div>
						</div>
					</div>

					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-4 parsley-row">
							<label class="uk-form-label">Course Type<span class="req">*</span></label>
							<span class="icheck-inline">
								<input type="radio" id="radio_free" name="courseType" ng-model="nomination.courseType" icheck required value="Free"/>
								<label for="radio_free" class="inline-label">Free</label>
							</span>
							<span class="icheck-inline">
								<input type="radio" id="radio_paid" name="courseType" ng-model="nomination.courseType" icheck value="Paid"/>
								<label for="radio_paid" class="inline-label">Paid</label>
							</span>
						</div>
						<div class="uk-width-medium-1-4 parsley-row">
							<label class="uk-form-label" style="margin-bottom:-4px">Content Provider<span class="req">*</span></label>
							<select
								kendo-drop-down-list
								style="width: 100%"
								k-option-label="'--Select--'"
								k-data-source="contentProviders"
								k-data-value-field="'id'"
								k-data-text-field="'name'"
								k-filter="'contains'"
								ng-model="nomination.contentProviderId"
								required
							></select>
						</div>
						<div class="uk-width-medium-1-4 parsley-row">
							<label class="uk-form-label" style="margin-bottom:-4px">Start date<span class="req">*</span></label>
							<input 
								style="box-shadow:none !important;width:100%;"
								kendo-date-picker
								placeholder="From"
								k-ng-model="startDate"
								ng-model="nomination.startDate"
								k-min="maxDateOfFrom"
								k-rebind="maxDateOfFrom"
								k-format="'dd-MM-yyyy'"
								k-on-change="fromDateChanged(startDate)"
								required
							/>
						</div>
						<div class="uk-width-medium-1-4 parsley-row">
							<label class="uk-form-label" style="margin-bottom:-4px">End date<span class="req">*</span></label>
							<input 
								style="box-shadow:none !important;width:100%;"
								kendo-date-picker
								placeholder="To"
								ng-model="nomination.endDate"
								k-min = "minDateOfTo"
								k-rebind = "minDateOfTo"
								k-format="'dd-MM-yyyy'"
								required
							/>
						</div>
					</div>

					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-medium-1-3 parsley-row" style="padding-top:12px">
							<label for="duration">Course duration (as mentioned on website)<span class="req">*</span></label>
							<input id="duration" type="text" required  class="md-input" md-input ng-model="nomination.duration"/>
						</div>
						<div class="uk-width-medium-1-3 parsley-row" ng-show="nomination.courseType == 'Paid'">
							<label class="uk-form-label">Payment type<span class="req">*</span></label>
							<span class="icheck-inline">
								<input type="radio" id="radio_self" name="paidBy" ng-model="nomination.paidBy" icheck ng-required="nomination.courseType == 'Paid'" value="Self" />
								<label for="radio_self" class="inline-label">Self</label>
							</span>
							<span class="icheck-inline">
								<input type="radio" id="radio_company" name="paidBy" ng-model="nomination.paidBy" icheck value="Company" />
								<label for="radio_company" class="inline-label">Company</label>
							</span>
						</div>
						<div class="uk-width-medium-1-3 parsley-row" style="padding-top:12px" ng-show="nomination.courseType == 'Paid'">
							<label>Total cost (in INR)<span class="req">*</span></label>
							<input
								class="md-input masked_input"
								type="text"
								name="courseCost"
								data-inputmask="'alias': 'numeric', 'digits': 2, 'digitsOptional': false, 'placeholder': '0'"
								data-inputmask-showmaskonhover="false"
								md-input
								ng-required="nomination.courseType == 'Paid'"
								ng-model="nomination.courseCost"
								ng-change="checkInstallmentAmount(nomination.courseCost, nomination.installments, formNomination)"
							/>
						</div>
					</div>

					<div class="uk-grid" data-uk-grid-margin ng-show="nomination.courseType == 'Paid'">
						<div class="uk-width-medium-1-3 parsley-row">
							<label class="uk-form-label" style="margin-bottom:-4px">Payment frequency as per course provider<span class="req">*</span></label>
							<select
								kendo-drop-down-list
								style="width: 100%"
								k-option-label="'--Select--'"
								k-data-source="paymentFrequency"
								k-data-value-field="'id'"
								k-data-text-field="'value'"
								k-filter="'contains'"
								ng-model="nomination.paymentFrequency"
								ng-required="nomination.courseType == 'Paid'"
								ng-change="enableSpecificPayment()"
							></select>
						</div>
						<div class="uk-width-medium-1-3 parsley-row" style="padding-top:12px" ng-show="nomination.paymentFrequency == 'Others'">
							<label>Specify the payment frequency<span class="req">*</span></label>
							<input
								type="text"
								class="md-input"
								maxlength="100"
								md-input
								ng-model="nomination.otherPaymentOption"
								ng-required="nomination.paymentFrequency == 'Others'"
							/>
						</div>
						<div class="uk-width-medium-1-3 parsley-row">
							<label class="uk-form-label" style="margin-bottom:-4px">Installments<span class="req">*</span></label>
							<input
								style="width: 100%;"
								kendo-numeric-text-box
								k-min="0"
								k-max="12"
								k-options="numericText"
								ng-model="nomination.noOfInstallments"
								ng-change="installmentRange(nomination.installments, nomination.noOfInstallments);"
								ng-required="nomination.courseType == 'Paid'"
								ng-readonly="nomination.paymentFrequency == 'Single'"
							/>
						</div>
					</div>

					<h3 ng-show="nomination.installments.length" class="heading_a uk-margin-medium-top">Installments</h3>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-grid uk-width-medium-1-2 uk-margin-small-top" ng-repeat="item in nomination.installments">
							<div class="uk-width-medium-1-2">
								<div class="uk-input-group">
									<span class="uk-input-group-addon">{{$index + 1}}</span>
									<div class="parsley-row">
										<label ng-show="$first || $index == 1">Amount</label>
										<input
											style="width: 100%;"
											kendo-numeric-text-box
											k-min="1"
											k-max="nomination.courseCost"
											k-rebind="nomination.courseCost"
											ng-required="nomination.courseType == 'Paid'"
											k-options="numericText"
											ng-model="item.amount"
											ng-change="checkInstallmentAmount(nomination.courseCost, nomination.installments, formNomination)"
										/>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-2 parsley-row">
								<label ng-show="$first || $index == 1">Choose salary cycle</label>
								<input kendo-date-picker
									k-options="monthSelectorOptions"
									k-format="'MMMM yyyy'"
									style="width: 100%;"
									ng-required="nomination.courseType == 'Paid'"
									ng-model="item.actualProcessingDate"
								/>
							</div>
						</div>

						<div class="uk-grid uk-width-medium-1-1 uk-margin-small-top" ng-show="nomination.installments.length">
							<span ng-show="showCase.installmentHasError" class="uk-text-danger">
								* Total Cost is not matching with the sum of all the installment amount.
							</span>
						</div>
					</div>

					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-1-1">
							<div class="parsley-row">
								<label for="message">Reason for nomination</label>
								<textarea
									class="md-input"
									cols="10"
									rows="2"
									required
									md-input
									ng-model="nomination.comments"
									data-parsley-trigger="keyup"
									data-parsley-minlength="100"
									data-parsley-maxlength="250"
									data-parsley-validation-threshold="10"
								></textarea>
							</div>
						</div>
					</div>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-1-1">
							<button type="submit" ng-click="showCase.updateNomination(nomination, formNomination);" class="md-btn md-btn-primary">Update</button>
						</div>
					</div>
				</form>
			</div>
		</div>

    </div>
</div>
<style>
		

    * {
      box-sizing: border-box;
    }
	.cprovider{
		width:100%;
	}
	
    .slider {
        width: 100%;
        margin: 0px auto 20px auto; 
		max-width: 1730px;
		padding:0 20px;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }
	.highlight:hover {
			color:#7CB342;
			cursor:pointer;
		}
		.app_theme_a .highlight:hover {
			color:#FF4081;
		}
		.app_theme_b .highlight:hover {
			color:#00BCD4;
		}
		.app_theme_c .highlight:hover {
			color:#1E88E5;
		}
		.app_theme_d .highlight:hover {
			color:#673AB7;
		}
		.app_theme_e .highlight:hover {
			color:#E53935;
		}
		.app_theme_f .highlight:hover {
			color:#7CB342;
		}
	.uk-datepicker {z-index:1400!important;}
	.loader {
	    border: 5px solid #f3f3f3; /* Light grey */
	    border-top: 5px solid #3498db; /* Blue */
	    border-radius: 50%;
	    margin: 10px auto;
	    position: relative;
	    width: 30px;
	    height: 30px;
	    animation: spin 1s linear infinite;
	}
    @keyframes spin {
	    0% { transform: rotate(0deg); }
	    100% { transform: rotate(360deg); }
	}
</style>


