<div id="page_content">
    <div id="page_content_inner">
        <div class="md-card uk-width-1 uk-container-center">
            <div class="md-card-content large-padding">
				<div class="md-card uk-margin-medium-bottom">
					<div class="md-card-content">
                        <h3 class="heading_a">Upload Documents</h3>
                        <br />
                        <br />
                        <div  ng-show="enableUploadOption">
                            <form ng-submit="uploadReceipt($event);" name="uploadDocument" id="uploadDocument" class="uk-form-stacked" enctype="multipart/form-data" novalidate>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <span class="uk-form-help-block" style="margin:-12px 0 0;">Select the File type<span class="req">*</span></span>
                                        <input
                                            id="filetype"
                                            name="filetype"
                                            style="width: 100%;"
                                            kendo-drop-down-list
                                            k-options="k_field_types"
                                            k-rebind="k_field_types"
                                            ng-model="info.fileType"
                                            required 
                                            data-parsley-required-message="Please select file type"
                                        />
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <span class="uk-form-help-block" style="margin:-12px 0 0;">Select the Course<span class="req">*</span></span>
                                        <input
                                            id="approved_course"
                                            name="approved_course"
                                            style="width: 100%;"
                                            kendo-drop-down-list
                                            k-options="k_approved_course"
                                            k-rebind="k_approved_course"
                                            ng-model="info.approved_course"
                                            required 
                                            data-parsley-required-message="Please select file type"
                                        />
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <div class="k-button k-upload-button" aria-label="Select files...">
                                            <input
                                                type="file"
                                                class="md-input dropify"
                                                id="empDataFile"
                                                required 
                                                file-model="empDataFile"
                                                accept=".jpg, .png"
                                            />
                                            <span>Select files</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">								
                                        <button style="margin-left:29px;" type="submit"  class="md-btn md-btn-primary md-btn-small" >Upload</button>
                                    </div>
                                </div>	
                            </form>
                        </div>
                        <div  ng-show="!enableUploadOption">
                            <div style="text-align: center;"><p>No approved nominations available to upload documents.</p></div>
                        </div>
					</div>
				</div>
                <div class="md-card uk-margin-medium-bottom">
					<div class="md-card-content">
						<table 
    						id="nomination_list" 
    						datatable 
    						dt-options="nominatedEmployees.dtOptions" 
    						dt-columns="nominatedEmployees.dtColumns" 
    						dt-instance="dtNomInstance" 
    						class="uk-table uk-table-condensed uk-table-hover" 
    						cellspacing="0" 
    						width="100%"
                        ></table>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

<!-- Start of View Document Dialog -->
<div class="uk-modal" id="documentDialog">
	<div class="uk-modal-dialog uk-modal-dialog-large"> 
		<button type="button" class="uk-modal-close uk-close"></button>
		<div class="uk-width-1 uk-container-center">
			<div class="md-card-content large-padding">
				<span class="uk-alert ng-scope">Nomination Details</span>
			</div>
			<div class="documentDiv">
                <form id="emp_nomination_upd_form" class="uk-form-stacked" novalidate>                    
                    <div class="custom-form">                        
                        <div class="custom_header">
                            Course Document
                        </div>
                        <!-- Employee Details -->
                        <div class="uk-grid uk-grid-width-small-1-1 uk-grid-width-medium-1-1 uk-grid-width-large-1-1" data-uk-grid-margin>
                            <div class="parsley-row">
								<label>&nbsp;</label>
                                <label for="employeeID">Employee Name<span class="req">*</span></label>
                                <input 
                                    type="text" 
                                    id="EmployeePID" 
                                    class="md-input" 
                                    maxlength="100" 
                                    ng-model="documentsList.course.employeeFullName" 
                                    required 
                                    md-input
                                />
                            </div>
                        </div>
                        <!-- Employee Details -->
                        <div class="uk-grid uk-grid-width-small-1-1 uk-grid-width-medium-1-1 uk-grid-width-large-1-1" data-uk-grid-margin>
                            <div class="parsley-row" ng-show="documentsList.course.docUrl">
								<img src="receipts/{{documentsList.course.employeeID}}/{{documentsList.course.docUrl}}" alt="{{documentsList.course.docName}}" style="max-width: 600px;" />
                            </div>                                
                        </div>
                        <!-- Employee Details -->
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                            <button type="button" ng-show="userType == 'Self' && approveStatus == 2"  style="float:right;margin-left:1%;" ng-click="updateNominationByEmp(nominations.nominationID)" class="clsButtonUpload md-btn md-btn-warning">ACCEPT</button>
                                <button type="button" ng-show="isDivHead == 0 && userType != 'Self' && (approveStatus == 1 || approveStatus == 3)"  style="float:right;margin-left:1%;" ng-click="updateNominationByRM(nominations.nominationID)" class="clsButtonUpload md-btn md-btn-warning">FORWARD</button>
                                <button type="button" ng-show="isDivHead == 1 && approveStatus == 4"  style="float:right;margin-left:1%;" ng-click="updateNominationByDH(nominations.nominationID, 2)" class="clsButtonUpload md-btn md-btn-warning">REJECT</button>
                                <button type="button" ng-show="isDivHead == 1 && approveStatus == 4" style="float:right;" id="nominateEmployee" ng-click="updateNominationByDH(nominations.nominationID, 1)" class="md-btn md-btn-primary">APPROVE</button>                                    
                            </div>
                        </div>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End of View Document Dialog -->

<style>
	.uk-datepicker {z-index:1400!important;}
	.loader {
	    border: 5px solid #f3f3f3; /* Light grey */
	    border-top: 5px solid #3498db; /* Blue */
	    border-radius: 50%;
	    margin: 10px auto;
	    position: relative;
	    width: 30px;
	    height: 30px;
	    animation: spin 1s linear infinite;
	}

	@keyframes spin {
	    0% { transform: rotate(0deg); }
	    100% { transform: rotate(360deg); }
	}
</style>