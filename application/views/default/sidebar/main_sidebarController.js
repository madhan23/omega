angular
    .module('meritOmegaApp')
    .controller('main_sidebarCtrl', [
        '$timeout',
        '$scope',
        '$rootScope',
        '$window',
        '$state',
        '$http',
        'userSession',
        function ($timeout,$scope,$rootScope,$window,$state,$http,userSession) {
			$scope.accessType = userSession.get('AccessType');
			$scope.$on('onLastRepeat', function (scope, element, attrs) {
                $timeout(function() {
                    if(!$rootScope.miniSidebarActive) {
                        // activate current section
                        $('#sidebar_main').find('.current_section > a').trigger('click');
                    } else {
                        // add tooltips to mini sidebar
                        var tooltip_elem = $('#sidebar_main').find('.menu_tooltip');
                        tooltip_elem.each(function() {
                            var $this = $(this);

                            $this.attr('title',$this.find('.menu_title').text());
                            UIkit.tooltip($this, {});
                        });
                    }
                })
            });

            // language switcher
            $scope.langSwitcherModel = 'gb';
            var langData = $scope.langSwitcherOptions = [
                {id: 1, title: 'English', value: 'gb'},
                {id: 2, title: 'French', value: 'fr'},
                {id: 3, title: 'Chinese', value: 'cn'},
                {id: 4, title: 'Dutch', value: 'nl'},
                {id: 5, title: 'Italian', value: 'it'},
                {id: 6, title: 'Spanish', value: 'es'},
                {id: 7, title: 'German', value: 'de'},
                {id: 8, title: 'Polish', value: 'pl'}
            ];
            $scope.langSwitcherConfig = {
                maxItems: 1,
                render: {
                    option: function(langData, escape) {
                        return  '<div class="option">' +
                            '<i class="item-icon flag-' + escape(langData.value).toUpperCase() + '"></i>' +
                            '<span>' + escape(langData.title) + '</span>' +
                            '</div>';
                    },
                    item: function(langData, escape) {
                        return '<div class="item"><i class="item-icon flag-' + escape(langData.value).toUpperCase() + '"></i></div>';
                    }
                },
                valueField: 'value',
                labelField: 'title',
                searchField: 'title',
                create: false,
                onInitialize: function(selectize) {
                    $('#lang_switcher').next().children('.selectize-input').find('input').attr('readonly',true);
                }
            };

            $scope.userType = '';

            $http({
                method:"POST", 
                url: 'dashboard/getTeamCount'
            })
            .then(function(response){
                if (response.data.error) {
                    $scope.userType = 'Self';
                }
                else {
                    var teamCount = response.data.count[0].TeamCount;
                    if(teamCount > 0) {
                        $scope.userType = 'Team';
                    }
                    else {
                        $scope.userType = 'Self';
                    }
                }
            },function(response){
                //$scope.userType = 'Self';
            });
            
            $http({
                url: 'dashboard/Check_Login',
                method: 'POST'
            })
            .then(function(response){
                if (response.data.error) {
                    $rootScope.AuthenticUser = undefined;
                }
                else {
                    if (!response.data.AuthenticUser || response.data.AuthenticUser == null)
                    {
                        $rootScope.AuthenticUser = undefined;
                    }
                    else if (response.data.AuthenticUser && response.data.AuthenticUser != null)//&& toState.url == '/login'
                    {
                        $rootScope.AuthenticUser = response.data.AuthenticUser;
                    }

					var AuthenticUser = $rootScope.AuthenticUser;
					var TeamCount = parseInt(AuthenticUser['TeamCount']);
                    if (AuthenticUser)
                    {
                        $scope.sections = [                            
                            {
                                id: 1,
                                title: 'Dashboard',
                                icon: '&#xE85C;',
                                link: 'restricted.nomination'
                            },                            
                            {
                                id: 3,
                                title: 'Self Nomination',
                                icon: '&#xE8D2;',
                                link: 'restricted.courseApplication'
                            }
                            ,                            
							/*
							{
                                id: 4,
                                title: 'Documents',
                                icon: '&#xE8D2;',
                                link: 'restricted.uploadDocuments'
							}
							*/
                        ];
                    }
                    if($scope.accessType == 'Admin' || $scope.accessType == 'DivisionHead' || $scope.accessType == 'ReportingManger') {
                        $scope.sections.push(
                            {
                                id: 2,
                                title: 'Crew Nomination',
                                icon: '&#xE8D3;',
                                link: 'restricted.crewnomination'
                            }
                        );
                    }
                    if($scope.accessType == 'Admin')
                    {
                        $scope.sections.push(
                            {
                                id: 4,
                                title: 'Admin Dashboard',
                                link: 'restricted.divisionWiseStatistics',
                                icon: '&#xE85C;'
                                /*,
                                submenu: [
                                    {
                                        title: 'Division wise statistics',
                                        link: 'restricted.divisionWiseStatistics',
                                        icon: '&#xE85C;'
                                    },
                                    {
                                        title: 'Division wise statistics',
                                        link: 'restricted.divisionWiseStatistics',
                                        icon: '&#xE85C;'
                                    }
                                ]*/
                            }
                        );

                    }
					/*
					if(TeamCount > 0) {
                        $scope.sections.push(
                            {
                                id: 2,
                                title: 'Nominate Employee',
                                icon: '&#xE24D;',
                                link: 'restricted.nominateEmployee'
                            }
                        );
					}
					*/
                }
            }, function(response){
                $rootScope.AuthenticUser = undefined;
            });
        }
    ])
