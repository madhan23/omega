    <!-- main header -->
    <header id="header_main" ng-controller="main_headerCtrl">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <div class="main_logo_top" ng-if="topMenuActive || fullHeaderActive">
                    <a ui-sref="restricted.dashboard"><img ng-src="assets/img/avatars/user.png" alt="" add-image-prop/></a>
                </div>
                <!-- main sidebar switch -->
                <sidebar-primary-toggle></sidebar-primary-toggle>
                <!-- secondary sidebar switch -->
                <!-- sidebar-secondary-toggle ng-class="{ 'uk-hidden-large': secondarySidebarHiddenLarge }"></sidebar-secondary-toggle -->
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li>
                            <a href="#" class="user_action_icon" full-screen-toggle>
                                <i class="material-icons md-24 md-light">&#xE5D0;</i>
                            </a>
                        </li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image">
                                Hi, &nbsp; {{AuthenticUser.EmployeeName}}
                                <img class="md-user-image" ng-src="assets/img/avatars/user.png" alt="" add-image-prop/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <li>
                                        <a href="dashboard/logout">Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header><!-- main header end -->
