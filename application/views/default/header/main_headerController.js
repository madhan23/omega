angular
    .module('meritOmegaApp')
    .controller('main_headerCtrl', [
        '$timeout',
        '$scope',
        '$window',
        '$state',
        '$http',
        function ($timeout,$scope,$rootScope,$window,$state,$http) {
            $('#menu_top').children('[data-uk-dropdown]').on('show.uk.dropdown', function(){
                $timeout(function() {
                    $($window).resize();
                },280)
            });
        }
    ]);