    <!-- header -->
    <!--div ng-controller="main_headerCtrl"></div-->

    <!-- restricted main content -->
    <div ui-view ng-class="{ 'uk-height-1-1': page_full_height }"></div>

    <!-- main sidebar -->
    <!--div ng-controller="main_sidebarCtrl"></div-->

    <!-- style switcher -->
    <!-- <div oc-lazy-load="['assets/css/style_switcher.min.css','assets/js/style_switcher/style_switcher.js']">
        <style-switcher></style-switcher>
    </div> -->
