<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">
<div class="loginbg" id="login_card">
	<div class="login animated bounceInDown delay-16s">
		<div class="logos">
				<a href="#" title="Opprtunity to Mould, Evolve and Grow Aspirations">
					<img src="assets/img/omega-logo.png" width="230" height="134" alt=""/>
				</a>
		</div>
		<h5 class="text-center md-color-red-A400 err_msg" ng-if="login_error" ng-bind-html="login_error">{{login_error}}</h5>
		<form method="post" id="login_form" ng-submit="UserLogin()">
			<input type="text" id="userid" class="rinpt" placeholder="Employee ID" id="login_username" autocomplete="off" required ng-model="User.EmployeeID">
			<input type="password" id="login_password" required ng-model="User.Password" class="rinpt" placeholder="Password">
			<input type="submit" class="sibtn" value="Signin">
		</form>
		<div class="powtxt"> Powered by <a href="https://www.meritgroup.co.uk/" target="_blank">Merit IT</a><br>
			Copyrights © Merit Group. All right reserved. 
		</div>
  </div>
</div>


<!--
<div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding">
                <div class="login_heading">
                    <div class="user_avatar"></div>
                </div>
                <h5 class="text-center md-color-red-A400 err_msg" ng-if="login_error" ng-bind-html="login_error">{{login_error}}</h5>
                <form method="post" id="login_form" ng-submit="UserLogin()">
                    <div class="uk-form-row">
						<div class="parsley-row">
							<label for="login_username">Employee ID</label>
							<input class="md-input" type="text" id="login_username" autocomplete="off" required ng-model="User.EmployeeID" md-input />
						</div>
                    </div>
                    <div class="uk-form-row">
						<div class="parsley-row">
							<label for="login_password">Password</label>
							<input class="md-input" type="password" id="login_password" required ng-model="User.Password" md-input />
						</div>
                    </div>
                    <div class="uk-margin-medium-top" style="text-align: center;">
                        <button type="submit" class="md-btn md-btn-primary">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
-->
	
<style>
	html{
		background: #fff !important;
	}
@charset "utf-8";
/* CSS Document */
@import url('https://fonts.googleapis.com/css?family=Roboto:400,500,700');
body {
	background: #fff url(assets/img/login-bg.jpg) no-repeat right bottom;
	padding: 0;
	margin: 0;
	font-family: 'Roboto', sans-serif;
}
:focus {
	outline-color: transparent;
	outline-style: none;
}
input:focus, select:focus, textarea:focus, button:focus {
	outline: none;
}
.loginbg {
	background: #fff url(assets/img/login-bg.jpg) no-repeat right bottom;
	height: 100vh;
	width: 100%;
	display: -webkit-box;
	display: -webkit-flex;
	display: -moz-box;
	display: -ms-flexbox;
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	padding: 15px;
	box-sizing: border-box;
	background-size: cover;
}
.login {
	width: 400px;
	background: #fff;
	border-radius: 6px;
	overflow: hidden;
	display: -webkit-box;
	display: -webkit-flex;
	display: -moz-box;
	display: -ms-flexbox;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
	padding: 40px 30px;
	box-shadow: 0px 0px 20px 0px rgba(51, 144, 206, 0.12);
	box-sizing: border-box;
	font-family: 'Roboto', sans-serif;
	position: relative;
	margin: 0;
}
.logos {
	text-align: center;
	width: 100%;
	margin: 0 0 25px 0
}
.rinpt {
	border: 1px solid rgb(232, 232, 232);
	background: #fff;
	margin: 0 0 23px 0;
	border-radius: 30px;
	padding: 11px 20px;
	width: 100%;
	font-family: 'Roboto', sans-serif;
	font-size: 14px;
	line-height: 16px;
	box-sizing: border-box;
	-webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	transition: all 0.2s ease-in-out;
	position: relative;
}
.sibtn {
	border: 0;
	background-image: -moz-linear-gradient( 0deg, rgb(191,43,68) 0%, rgb(141,44,128) 100%);
	background-image: -webkit-linear-gradient( 0deg, rgb(191,43,68) 0%, rgb(141,44,128) 100%);
	background-image: -ms-linear-gradient( 0deg, rgb(191,43,68) 0%, rgb(141,44,128) 100%);
	border-radius: 30px;
	padding: 12px 35px;
	width: 130px;
	font-family: 'Roboto', sans-serif;
	font-size: 14px;
	font-weight: 500;
	color: #fff;
	margin: 7px auto 25px auto;
	cursor: pointer;
	-webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	transition: all 0.2s ease-in-out;
	line-height: 16px;
	margin-left: 30%;
}
.sibtn:hover {
	width: 140px;
	-webkit-box-shadow: 0px 0px 5px 0px rgba(51, 144, 206, 0.12);
	-moz-box-shadow: 0px 0px 5px 0px rgba(51, 144, 206, 0.12);
	box-shadow: 0px 0px 5px 0px rgba(51, 144, 206, 0.12);
}
.rinpt:focus {
	border: 1px solid #d0e6f1;
	-webkit-box-shadow: 3px 3px 5px 0px rgba(51, 144, 206, 0.08);
	-moz-box-shadow: 3px 3px 5px 0px rgba(51, 144, 206, 0.08);
	box-shadow: 3px 3px 5px 0px rgba(51, 144, 206, 0.08);
}
.tchk {
	background: url(../img/ticks.jpg) no-repeat top right;
	-webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	transition: all 0.2s ease-in-out;
}
.tchkrt {
	background: url(../img/ticks.jpg) no-repeat bottom right;
}
.rinpt.error {
	border: 1px solid #eaa0a0;
}
.powtxt {
	color: #838383;
	text-align: center;
	font-size: 11px;
	line-height: 18px;
	width: 100%;
	margin: 10px 0;
}
.powtxt a {
	color: #b22f57;
	text-decoration: none;
}
.frgt {
	position: absolute;
	bottom: -30px;
	right: -30px;
	background: #f6fafd;
	border-radius: 50%;
	height: 70px;
	width: 50px;
	font-size: 14px;
	font-weight: 700;
	text-align: left;
	line-height: 47px;
	color: #79b1d3;
	cursor: pointer;
	border: 1px solid #eaf4fa;
	-webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	transition: all 0.2s ease-in-out;
	opacity: 0.9;
	padding: 0 0 0 20px;
}
.frgt:hover {
	color: #cb2c36;
	-ms-transform: scale(1.1, 1.1); /* IE 9 */
	-webkit-transform: scale(1.1, 1.1); /* Safari */
	transform: scale(1.1, 1.1);
	opacity: 1;
}
/*
@media (min-width: 768px) {
.login {
	margin: 0 2% 0 auto;
}
}


@media (min-width: 992px) {
}


@media (min-width: 1200px) {
.login {
	margin: 0 5% 0 auto;
}
}
*/
		.login_page		{
			margin: 0;
			padding: 0;
		}
		@keyframes shake-logo {
		16.65% {
			transform: translateX(8px);
		}
		33.3% {
			transform: translateX(-6px);
		}
		49.95% {
			transform: translateX(4px);
		}
		66.6% {
			transform: translateX(-2px);
		}
		83.25% {
			transform: translateX(1px);
		}
		100% {
			transform: translateX(0px);
		}
		}
		.err_msg {
			animation-duration: 1s;
			animation-iteration-count: 1;
			animation-name: shake-logo;
			animation-timing-function: ease-in-out;
			margin: 0 0 15px 0;
			text-align: center;
			width: 100%;
		}
	</style>