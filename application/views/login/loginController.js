angular
    .module('meritOmegaApp')
    .controller('loginCtrl', [
        '$scope',
        '$rootScope',
        '$state',
		'$http',
		'userSession',
        function ($scope,$rootScope,$state,$http, userSession) {
            $scope.User = {};
            $scope.UserLogin = function() {
				$('#login_form').parsley().validate();
                if ($('#login_form').parsley().isValid()) {
                    $http({
                        url: 'dashboard/getLogin',
                        method: 'POST',
                        data: $scope.User,
                    })
                    .then(function success(response) {
                        if (response.data.error) {
                            $rootScope.login_error = response.data.error;
                            $rootScope.AuthenticUser = undefined;
                        } else {
                            if (response.data.AuthenticUser && response.data.AuthenticUser != null) {
                                $rootScope.login_error = undefined;
								$rootScope.AuthenticUser = response.data.AuthenticUser;

								var userData = response.data.AuthenticUser;

								userSession.set('EmployeeID', userData.EmployeeID);
								userSession.set('AccessType', userData.accessType);

                                $state.go('restricted.nomination');
                            }
                        }
                    }, function failure(response){
                        $rootScope.login_error = response.statusText;
                        $rootScope.AuthenticUser = undefined;
                    });
                }
            };
        }
	]);
