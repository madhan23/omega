/*
*  Altair Admin AngularJS
*/
;"use strict";

var meritOmegaApp = angular.module('meritOmegaApp', [
    'ui.router',
    'oc.lazyLoad',
    'ngSanitize',	
    'ngAnimate',
    'ngRetina',
	'ConsoleLogger'
]);

meritOmegaApp.constant('variables', {
    header_main_height: 48,
    easing_swiftOut: [ 0.4,0,0.2,1 ],
    bez_easing_swiftOut: $.bez([ 0.4,0,0.2,1 ])
});

/* Run Block */
meritOmegaApp
    .run([
        '$rootScope',
        '$state',
        '$stateParams',
        '$http',
        '$window',
        '$timeout',
        'variables',
        'preloaders',
        'userSession',
		function ($rootScope, $state, $stateParams,$http,$window, $timeout,variables, userSession) {

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            $rootScope.$on('$stateChangeSuccess', function () {
                // scroll view to top
                $("html, body").animate({
                    scrollTop: 0
                }, 200);
				
                $timeout(function() {
                    $rootScope.pageLoading = false;
					$($window).resize();
                },300);

                $timeout(function() {
                    $rootScope.pageLoaded = true;
                    $rootScope.appInitialized = true;                    
                },600);

			});
			
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
				$http({
                    url: 'dashboard/Check_Login',
                    method: 'POST'
                })
                .then(function(response){
					if (response.data.error)
                    {
                        $rootScope.AuthenticUser = undefined;
                        $rootScope.login_error = response.data.error;
                        $state.go('login');
                    }
                    else
                    {
						if (!response.data.AuthenticUser || response.data.AuthenticUser == null)
                        {
                            $rootScope.AuthenticUser = undefined;
                            if (fromState.url != '/login' && fromState.url != '^' && fromState.url != '')
                            {
                                $rootScope.login_error = 'Session expired. Please login again';
                            }
                            else
                            {
                                $rootScope.login_error = undefined;
                            }
                            $state.go('login');
                        }
                        else if (response.data.AuthenticUser && response.data.AuthenticUser != null && toState.url == '/login')
                        {
							userSession.set('EmployeeID', response.data.AuthenticUser.EmployeeID);
							userSession.set('AccessType', response.data.AuthenticUser.accessType);

							$rootScope.AuthenticUser = response.data.AuthenticUser;

							$state.go('restricted.nomination');
                        }
                    }
                }, function(response){
                    $rootScope.AuthenticUser = undefined;
                    //$rootScope.AuthenticUser.error = response.statusText;
                    $state.go('login');
                });

                // main search
                $rootScope.mainSearchActive = false;
                // single card
                $rootScope.headerDoubleHeightActive = false;
                // top bar
                $rootScope.toBarActive = false;
                // page heading
                $rootScope.pageHeadingActive = false;
                // top menu
                $rootScope.topMenuActive = false;
                // full header
                $rootScope.fullHeaderActive = false;
                // full height
                $rootScope.page_full_height = false;
                // secondary sidebar
                $rootScope.sidebar_secondary = false;
                $rootScope.secondarySidebarHiddenLarge = false;

                if($($window).width() < 1220 ) {
                    // hide primary sidebar
                    $rootScope.primarySidebarActive = false;
                    $rootScope.hide_content_sidebar = false;
                }
                if(!toParams.hasOwnProperty('hidePreloader')) {
                    $rootScope.pageLoading = true;
                    $rootScope.pageLoaded = false;
                }
				
            });

            // fastclick (eliminate the 300ms delay between a physical tap and the firing of a click event on mobile browsers)
            FastClick.attach(document.body);

            // get version from package.json
            $http.get('./package.json').then(function onSuccess(response) {
                $rootScope.appVer = response.version;
            });

            // modernizr
            $rootScope.Modernizr = Modernizr;

            // get window width
            var w = angular.element($window);
            $rootScope.largeScreen = w.width() >= 1220;

            w.on('resize', function() {
                return $rootScope.largeScreen = w.width() >= 1220;
            });

            // show/hide main menu on page load
            $rootScope.primarySidebarOpen = ($rootScope.largeScreen) ? true : false;

            $rootScope.pageLoading = true;
        }
    ])
    .run([
        'PrintToConsole',
        function(PrintToConsole) {
            // app debug
            PrintToConsole.active = false;
        }
    ]);
;
