angular
    .module('meritOmegaApp')
    .controller('divisionWiseStatisticsCtrl', [
        '$rootScope',
		'$scope',
		'$compile',
		'DTOptionsBuilder',
		'DTColumnBuilder',
		'$timeout',
		'$resource',
		'$http',
		'userSession',
		'$window',
		'$state',
        
        function ($rootScope,$scope, $compile, DTOptionsBuilder, DTColumnBuilder, $timeout, $resource, $http, userSession, $window, $state) {
			$scope.monthSelectorOptions = {
				start: "decade",
				depth: "decade",
				format:"yyyy",
				max: new Date(),
				min: new Date("2017-01-01")
			};
			/*  DivisionWiseNominationCount chart starts */ 
            $http({
				url: 'dashboard/getDivisionWiseNominationCount',
				method: 'get'
			}).then(function success(response){				
                if(response.data.division_heads){
                    $timeout(function() {
                        // c3.js
                        var c3chart_bar_stacked_id = 'c3_chart_bar_stacked';
                        var c3chart_bar_stacked = c3.generate({
                            bindto: '#'+c3chart_bar_stacked_id,
                            data: {
                                columns: response.data.total_and_nominated,
                                type: 'bar'
                            },
                            axis: {
                                x: {
                                    type: 'category',
                                    categories: response.data.division_heads
                                }
                            },
                            color: {
                                pattern: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
                            }
                        });
    
                    });
                }else{
                    $('#c3_chart_bar_stacked').html("<span>No data found</span>");
                }   
			/*  DivisionWiseNominationCount chart ends */ 

			/*  DivisionWiseHeadsBudget chart starts */ 
            });
            $http({
				url: 'dashboard/getDivisionWiseHeadsBudget',
				method: 'get'
			}).then(function success(response){	
                    $timeout(function() {
                        // c3.js
                        var c3chart_bar_stacked = c3.generate({
                            bindto: '#c3_chart_bar_stacked_divisionBudget',
                            data: {
                                columns: response.data.divisionBudget,
                                type: 'bar'
                            },
                            axis: {
                                x: {
                                    type: 'category',
                                    categories: response.data.division_heads
                                }
                            },
                            color: {
                                pattern: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
                            }
                        });
    
                    });
			  });
			  /*  DivisionWiseHeadsBudget chart starts */ 

			  /*  DivisionWiseNomination status statistics chart starts */ 
			  $http({
				url: 'dashboard/getDivisionWiseNominationStatistics',
				method: 'get'
			}).then(function success(response){
                    $timeout(function() {
                        // c3.js
                        var c3chart_bar_stacked = c3.generate({
                            bindto: '#c3_chart_bar_stacked_nominationStatistics',
                            data: {
                                columns: response.data.statistics,
								type: 'bar'
                            },
                            axis: {
                                x: {
                                    type: 'category',
                                    categories: response.data.division_heads
                                }
                            },
                            color: {
                                pattern: ['#1f77b4', '#2ca02c', '#d62728', '#7f7f7f', '#ff7f0e', '#9467bd', '#8c564b', '#e377c2', '#bcbd22', '#17becf']
                            }
                        });
    
                    });
			  });
			  /*  DivisionWiseNomination status statistics chart ends */ 

			/*  overall ContentProviders chart starts */ 
            $http({
				url: 'dashboard/getContentProvidersCount',
				method: 'get'
			}).then(function success(response){

				var contentProvidersCount = response.data;
				
				$timeout(function() {
					// donut chart
					var contentProvidersChart = '#contentProvidersChart';

					if ( $(contentProvidersChart).length && contentProvidersCount) {
		
						var c3chart_donut = c3.generate({
							bindto: contentProvidersChart,
							data: {
								columns: contentProvidersCount.counts,
								type : 'donut'
							},
							donut: {
								title: "Total - " + contentProvidersCount.total,
								width: 40,
								label: {
									format: function (value) { return value; }
								} 
							},
							color: {
								pattern: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
							}
						});
					} else {
						$(contentProvidersChart).html("<span>No data found</span>");
					}
				},500);
			});
			/*  overall ContentProviders chart ends */ 

            var vm = this;
            vm.dtOptions = DTOptionsBuilder
				.fromFnPromise(function() {
					return $resource('dashboard/getExitEmpList').query().$promise;
				})
				.withDOM("<'dt-uikit-header'<'uk-grid'<'uk-width-medium-2-3'l><'uk-width-medium-1-3'f>>>" +
				"<'uk-overflow-container'rt>" +
				"<'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'i><'uk-width-medium-7-10'p>>>")
				.withPaginationType('full_numbers')
				.withOption('processing', true)
				.withOption('initComplete', function() {
					$timeout(function() {
						$compile($('.dt-uikit .md-input'))($scope);
					})
				})
				.withOption('createdRow', function(row) {
					// Recompiling so we can bind Angular directive to the DT
					$compile(angular.element(row).contents())($scope);
				})
				// Active Buttons extension
				.withButtons([
					{
						extend:    'excelHtml5',
						text:      '<i class="uk-icon-file-excel-o"></i> Export',
						titleAttr: ''
					}
				]);
			
			$.extend( true, $.fn.dataTable.defaults, {
				"lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
			});
            vm.dtInstance = {};
            vm.dtColumns = [
				DTColumnBuilder.newColumn('EmployeeID').withTitle('EmployeeID'),
                DTColumnBuilder.newColumn('Employee Name').withTitle('Employee Name'),
				DTColumnBuilder.newColumn('Course Start Date').withTitle('Course Start Date'),
				DTColumnBuilder.newColumn('Course End Date').withTitle('Course End Date'),
                DTColumnBuilder.newColumn('Resignation Applied On').withTitle('Resignation Applied On'),
                DTColumnBuilder.newColumn('Separation Type').withTitle('Separation Type'),
                DTColumnBuilder.newColumn('Nomination Status').withTitle('Nomination Status'),
				DTColumnBuilder.newColumn('Resignation Status').withTitle('Resignation Status')
			];
			
			
            vm.dtOptions1 = DTOptionsBuilder
				.fromFnPromise(function() {
					if(!$scope.Year){
						$scope.Year = 1;
					}
					return $resource('dashboard/getBudgetReport?Year='+$scope.Year+'').query().$promise.then(function(result){
						$scope.getBudgetReport = result;
						$scope.Year = $scope.getBudgetReport[0].Year;
						return result;
					});
				})
				.withDOM("<'dt-uikit-header'<'uk-grid'<'uk-width-medium-2-3'l><'uk-width-medium-1-3'f>>>" +
				"<'uk-overflow-container'rt>" +
				"<'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'i><'uk-width-medium-7-10'p>>>")
				.withPaginationType('full_numbers')
				.withOption('processing', true)
				.withOption('initComplete', function() {
					$timeout(function() {
						$compile($('.dt-uikit .md-input'))($scope);
					})
				})
				.withOption('createdRow', function(row) {
					// Recompiling so we can bind Angular directive to the DT
					$compile(angular.element(row).contents())($scope);
				})
				// Active Buttons extension
				.withButtons([
					{
						extend:    'excelHtml5',
						text:      '<i class="uk-icon-file-excel-o"></i> Export',
						titleAttr: ''
					}
				]);
			
			$.extend( true, $.fn.dataTable.defaults, {
				"lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
			});
			vm.newPromise = newPromise;
			vm.reloadData = reloadData;
            vm.dtInstance1 = {};
            vm.dtColumns1 = [
				DTColumnBuilder.newColumn('EmployeeID').withTitle('EmployeeID'),
				DTColumnBuilder.newColumn('Employee Name').withTitle('Employee Name'),
				DTColumnBuilder.newColumn('From Date').withTitle('From Date'),
                DTColumnBuilder.newColumn('To Date').withTitle('To Date'),
                DTColumnBuilder.newColumn('Allocated').withTitle('Budget Allocated').withClass('dt-right'),
                DTColumnBuilder.newColumn('Utilized').withTitle('Budget Utilized').withClass('dt-right'),
                DTColumnBuilder.newColumn('Balance').withTitle('Balance Amount').withClass('dt-right'),
                DTColumnBuilder.newColumn('IsActive').withTitle('IsActive').withClass('dt-center')
			];
			function newPromise() {
				$rootScope.content_preloader_show('spinner');
				return $resource('dashboard/getBudgetReport?Year='+$scope.Year+'').query().$promise.then(function(result){
					$rootScope.content_preloader_hide('spinner');
					$scope.getBudgetReport = '';
					if(result.length > 0){
						$scope.getBudgetReport = result;
					}
					return result;
				});
			}
			function reloadData() {
				var resetPaging = true;
				vm.dtInstance.reloadData(callback, resetPaging);
			}

			function callback(json) {
				console.log(json);
			}
			$scope.refresh = function(){
				$state.reload();
			};
        }
    ])