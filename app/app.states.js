meritOmegaApp
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
            $urlRouterProvider
                .when('/', '/login')
                .otherwise('/');

            $stateProvider
            // -- ERROR PAGES --
                .state("error", {
                    url: "/error",
                    templateUrl: 'app/views/error.html'
                })
                .state("error.404", {
                    url: "/404",
                    templateUrl: 'app/components/pages/error_404View.html'
                })
                .state("error.500", {
                    url: "/500",
                    templateUrl: 'app/components/pages/error_500View.html'
                })
            // -- LOGIN PAGE --
                .state("login", {
					url: "/login",
                    templateUrl: 'dashboard/login_view',
                    controller: 'loginCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_iCheck',
								'lazy_parsleyjs',
                                'dashboard/login/loginController.js'
                            ]);
                        }]
                    }
                })
            // -- RESTRICTED --
                .state("restricted", {
                    abstract: true,
                    url: "",
                    views: {
                        'main_header': {
							templateUrl: 'dashboard/header',
                            controller: 'main_headerCtrl'
                        },
                        'main_sidebar': {
                            templateUrl: 'dashboard/sidebar',
                            controller: 'main_sidebarCtrl'
                        },
                        '': {
                            templateUrl: 'dashboard/restricted'
                        }
                    },
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_uikit',
                                'lazy_selectizeJS',
                                'lazy_switchery',
                                'lazy_prismJS',
                                'lazy_autosize',
								'lazy_iCheck',
								'lazy_KendoUI',                               
                                'dashboard/headerCtrl/headerController.js',
                                'dashboard/sidebarCtrl/main_sidebarController.js',
                            ],{ serie: true });
                        }]
                    }
                })
                .state("restricted.nomination", {
					url: "/nomination",
                    templateUrl: 'dashboard/nomination_view',
                    controller: 'nominationCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
								'lazy_parsleyjs',
								'lazy_KendoUI',
								'lazy_datatables',
								'lazy_charts_chartist',
                                'lazy_charts_c3',
								'bower_components/angular-resource/angular-resource.min.js',
                                'dashboard/nomination/nominationController.js'
                            ], {serie: true} );
                        }],
                        teamCount: function($http) {
                            return $http({method:"POST", url: 'dashboard/getTeamCount'})
                                .then(function(response){
                                    return response;
                                },function(response){
                                    return response;
                                });
                        },
                        accessStatus: function($http) {
                            return $http({method:"POST", url: 'dashboard/getAccessStatus'})
                                .then(function(response){
                                    return response;
                                },function(response){
                                    return response;
                                });
                        },
                    },
                    data: {
                        pageTitle: 'Nominations'
                    }
                })
                .state("restricted.nominateEmployee", {
					url: "/nominateEmployee",
                    templateUrl: 'dashboard/nominateEmployee_view',
                    controller: 'nominateEmployeeCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
								'lazy_parsleyjs',
								'lazy_KendoUI',
								'lazy_datatables',
								'bower_components/angular-resource/angular-resource.min.js',
                                'dashboard/nominateEmployee/nominateEmployeeController.js'
                            ], {serie: true} );
                        }],
                        employeeDeps: function($http) {
							return $http({method:"POST", url:'dashboard/getEmployeeDeps'})
								.then(function (response) {
									return response;
								}, function(response){
                                    return response;
                                });
						},
                    },
                    data: {
                        pageTitle: 'Employee Nomination'
                    }
                })
                .state("restricted.courseApplication", {
                    url: "/courseApplication",
                    templateUrl: 'dashboard/courseApplication_view',
                    controller: 'courseApplicationCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
								'lazy_parsleyjs',
								'lazy_masked_inputs',                                
								'lazy_KendoUI',
								'lazy_datatables',
                                'lazy_wizard',
                                'dashboard/courseApplication/courseApplicationController.js'
                            ], {serie:true});
                        }],
                        employeeDeps: function($http) {
							return $http({method:"POST", url:'dashboard/getEmployeeDeps'})
								.then(function (response) {
									return response;
								}, function(response){
                                    return response;
                                });
						},
                    },
                    data: {
                        pageTitle: 'Wizard'
                    }
                })
                .state("restricted.crewnomination", {
                    url: "/crewNomination",
                    templateUrl: 'app/components/crew/crewNomination.html',
                    controller: 'crewNominationCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
								'lazy_parsleyjs',
								'lazy_masked_inputs',                                
								'lazy_KendoUI',
								'lazy_datatables',
                                'lazy_wizard',
                                'app/components/crew/crewNominationController.js'
                            ], {serie:true});
                        }],
                        employeeDeps: function($http) {
							return $http({method:"POST", url:'dashboard/getEmployeeDeps'})
								.then(function (response) {
									return response;
								}, function(response){
                                    return response;
                                });
						},
                    },
                    data: {
                        pageTitle: 'Crew Nomination'
                    }
                })
                .state("restricted.divisionWiseStatistics",{
                    url: "/divisionWiseStatistics",
                    templateUrl: 'app/components/reports/divisionWiseStatistics.html',
                    controller: 'divisionWiseStatisticsCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_parsleyjs',
								'lazy_KendoUI',
								'lazy_datatables',
								'lazy_charts_chartist',
                                'lazy_charts_c3',
                                'bower_components/angular-resource/angular-resource.min.js',
								'app/components/reports/divisionWiseStatisticsController.js'
                            ], {serie:true});
                        }],
                    },
                    data: {
                        pageTitle: 'Admin Dashboard'
                    }

                })
				/*
                .state("restricted.uploadDocuments", {
                    url: "/uploadDocuments",
                    templateUrl: 'dashboard/uploadDocuments_view',
                    controller: 'uploadDocumentsCtrl',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'lazy_parsleyjs',                                
								'lazy_KendoUI',
								'lazy_datatables',
                                'lazy_wizard',                                
								'bower_components/angular-resource/angular-resource.min.js',
                                'dashboard/uploadDocuments/uploadDocumentsController.js'
                            ], {serie:true});
                        }],
                        teamCount: function($http) {
                            return $http({method:"POST", url: 'dashboard/getTeamCount'})
                                .then(function(response){
                                    return response;
                                },function(response){
                                    return response;
                                });
                        },
                        accessStatus: function($http) {
                            return $http({method:"POST", url: 'dashboard/getAccessStatus'})
                                .then(function(response){
                                    return response;
                                },function(response){
                                    return response;
                                });
                        },
                    },
                    data: {
                        pageTitle: 'Wizard'
                    }
				})
				*/
            }
    ]);
